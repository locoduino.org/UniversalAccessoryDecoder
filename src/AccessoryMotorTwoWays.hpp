//-------------------------------------------------------------------
#ifndef __accessoryMotorTwoWays_H__
#define __accessoryMotorTwoWays_H__
//-------------------------------------------------------------------

#include "AccessoryMotor.hpp"

//-------------------------------------------------------------------
#ifndef NO_MOTOR
#ifndef NO_MOTORTWOWAYS

#define MOTOR2WAYS(list, nb)	((AccessoryMotorTwoWays *) (list)[nb])

class AccessoryMotorTwoWays : public AccessoryMotor
{
	protected:

	public:
		AccessoryMotorTwoWays(int inDccId, byte inDccIdAccessory, unsigned long inDurationMilli = 0);
		AccessoryMotorTwoWays(int inDccIdLeft, byte inDccIdAccessoryLeft, int inDccIdRight, byte inDccIdAccessoryRight, unsigned long inDurationMilli = 0);
};
#endif

//-------------------------------------------------------------------
#endif
#endif
//-------------------------------------------------------------------
