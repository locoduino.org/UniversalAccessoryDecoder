//-------------------------------------------------------------------
#ifndef __buttonsCommanderSwitch_H__
#define __buttonsCommanderSwitch_H__
//-------------------------------------------------------------------

#ifdef UAD_VC
#include "../VStudio/arduino2.hpp"
#else
#include "arduino2.hpp"
#endif
#include "ButtonsCommanderButton.hpp"

#define SWITCH(list, nb)	((ButtonsCommanderSwitch *) list[nb])

struct DccIdPin
{
	int Decoder;
	byte Accessory;
	GPIO_pin_t Pin;

	int buttonState;       // the current reading from the input pin
	int lastButtonState;   // the previous reading from the input pin
	unsigned long lastDebounceTime;  // the last time the output pin was toggled
};

class ButtonsCommanderSwitch : public ButtonsCommanderButton
{
 private:
	unsigned long debounceDelay;    // the debounce time; increase if the output flickers
	
	byte dccIdSize;
	byte dccIdAddCounter;
	byte dccIdLoopCounter;
	DccIdPin *pDccId;
	int dccIdState;

public:
	ButtonsCommanderSwitch(byte inDccIdNumber);
	
	void Setup();
	void AddDccId(int inDccIdDecoder, byte inDccIdAccessory, int inPin);
	bool Loop();
	int GetDccIdDecoder() const;
	byte GetDccIdAccessory() const;
};

//-------------------------------------------------------------------
#endif
//-------------------------------------------------------------------
