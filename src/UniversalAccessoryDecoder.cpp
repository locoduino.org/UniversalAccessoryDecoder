/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Base functions of the library>
*************************************************************/

#include "UniversalAccessoryDecoder.h"

void UAD_StartSetup()
{
#ifdef DEBUG_MODE
	// Done by the SerialCommander setup...
	Serial.begin(115200);
	// Just for let the time to the PIC to initialize internals...
	delay(500);

	Serial.println(F(""));
	Serial.println(F("Universal Accessories Decoder V4.35."));
	Serial.println(F("Developed by Thierry Paris."));
	Serial.println(F(""));

	Serial.println(F("*** Setup started."));
#endif
}

void UAD_EndSetup()
{
#ifdef DEBUG_MODE
	Serial.println(F("*** Setup Finished."));
#endif
}
