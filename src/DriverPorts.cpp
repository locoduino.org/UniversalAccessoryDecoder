/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Class for a drive port list>
*************************************************************/

#include "UniversalAccessoryDecoder.h"

#ifdef DEBUG_MODE
void DriverPorts::CheckIndex(unsigned char inIndex, const __FlashStringHelper *inFunc)
{
	if (this->size == 0)
	{
		Serial.print(F("Size undefined in "));
		Serial.println(inFunc);
	}
	else
	if (inIndex < 0 || inIndex >= this->size)
	{
		Serial.print(F("Index error in "));
		Serial.println(inFunc);
	}
}
#endif

DriverPorts::DriverPorts()
{
	this->size = 0;
	this->pList = 0;
	this->addCounter = 0;
}

void DriverPorts::Setup(unsigned char inSize)
{
	this->size = inSize;
	this->addCounter = 0;
	this->pList = new DriverPort*[inSize];

	for (unsigned char i = 0; i < this->size; i++)
	{
		this->pList[i] = 0;
	}
}

void DriverPorts::Set(unsigned char inIndex, DriverPort *pAcc)
{
	CHECKPORTINDEX(inIndex, "DriverPorts::Set");
	this->pList[inIndex] = pAcc;
}

// Returns the index of the new added accessory.
unsigned char DriverPorts::Add(DriverPort *pAcc)
{
	CHECKPORTINDEX(addCounter, "DriverPorts::Add");
	this->pList[addCounter++] = pAcc;

	return addCounter - 1;
}

DriverPort *DriverPorts::operator[](unsigned char inIndex)
{
	CHECKPORTINDEX(inIndex, "DriverPorts::operator[]");
	return this->pList[inIndex];
}
