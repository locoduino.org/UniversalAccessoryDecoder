//-------------------------------------------------------------------
#ifndef __accessory_H__
#define __accessory_H__
//-------------------------------------------------------------------

#include "Arduino.h"

//-------------------------------------------------------------------

#ifdef DEBUG_MODE
#define CHECKDCC(val, acc, text)	CheckDccId(val, acc, F(text))
#else
#define CHECKDCC(val, text)
#endif

enum ACCESSORYTYPE { ACCESSORYUNDEFINED, ACCESSORYMOTOR, ACCESSORYLIGHT, ACCESSORYLIGHTMULTI, ACCESSORYSERVO, ACCESSORYSTEPPER };
enum ACC_STATE { STATE_NONE = 0, STATE_FIRST = 10000, STATE_SECOND = 20000};

struct DccPosition
{
	int DccId;
	byte DccIdAccessory;
	int Position;
};

class Accessory
{
private:
	byte dccPositionsSize;
	byte dccPositionsAddCounter;
	DccPosition *pDccPositions;
	unsigned long duration;
	unsigned long startingMillis;
	DccPosition lastDccPosition;
	unsigned int debounceDelay;
	unsigned long lastMoveTime;
	
protected:
	ACC_STATE state;
	ACC_STATE prevState;
	bool useStateNone;

	int dccIdCounter;
	ACCESSORYTYPE type;

public:
	Accessory(int inDccIdDecoder, byte inDccIdAccessory, bool inUseStateNone, unsigned long inDuration = 0);

	inline ACCESSORYTYPE GetAccessoryType() const { return this->type; }

	inline ACC_STATE GetState() const { return this->state; }
	inline ACC_STATE GetPreviousState() const { return this->prevState; }

	inline bool IsNone() const { return this->state == STATE_NONE; }
	inline bool IsFirst() const { return this->state == STATE_FIRST; }
	inline bool IsSecond() const { return this->state == STATE_SECOND; }
	inline bool UseStateNone() const { return this->useStateNone; }

	unsigned char AddDccPosition(int inDccIdMin, byte inDccIdAccessoryMin, int inPosition);
	int GetDccPosition(int inDccId, byte inDccAccessory);
	int GetDccPositionIndex(int inDccId, byte inDccAccessory);
	inline const DccPosition &GetLastDccPosition() const { return this->lastDccPosition; }
	inline const byte GetDccPositionSize() const { return this->dccPositionsSize; }
	inline void SetLastDccPosition(int inDccId, byte inDccIdAccessory) { this->lastDccPosition.DccId = inDccId; this->lastDccPosition.DccIdAccessory = inDccIdAccessory; }
	inline void SetLastDccPosition(int inDccId, byte inDccIdAccessory, int inPosition) { this->lastDccPosition.DccId = inDccId; this->lastDccPosition.DccIdAccessory = inDccIdAccessory; this->lastDccPosition.Position = inPosition; }
	void AdjustDccPositionsSize(int inNewSize);
	void ClearDccPositions();

	inline unsigned int GetDebounceDelay() const { return this->debounceDelay; }
	inline void SetDebounceDelay(unsigned int inDebounceDelay) { this->debounceDelay = inDebounceDelay; }

	inline unsigned long GetLastMoveTime() const { return this->lastMoveTime; }
	inline void SetLastMoveTime() { this->lastMoveTime = millis(); }

	inline unsigned long GetDuration() const { return this->duration; }
	inline void SetDuration(unsigned long inDuration) { this->duration = inDuration; }

	inline unsigned long GetActionStartingMillis() const { return this->startingMillis; }
	virtual void StartAction();
	virtual void StartAction(ACC_STATE inState);
	virtual void ResetAction();
	virtual bool ActionEnded();	
	inline bool IsActionPending() const	{ return this->startingMillis > 0; }
	inline virtual bool IsGroupActionPending() const	{ return this->IsActionPending(); }

	bool Loop();
	virtual void Move(int inDccId, byte inDccAccessory) = 0;

	virtual ACC_STATE Toggle() = 0;
	inline virtual bool CanBePositionnal() const { return false; }
	inline virtual void MovePosition(int inPosition) {}

	inline bool IsEmpty() const { return this->pDccPositions == 0; }
		
	virtual void Setup(ACC_STATE inStartingState = STATE_NONE);
	inline virtual void SetState(ACC_STATE inNewState) { this->state = inNewState; }

#ifdef DEBUG_MODE
	static void CheckDccId(int inDccId, byte inDccIdAccessory, const __FlashStringHelper *infunc);
#endif
protected:
	inline void SetStartingMillis() { this->startingMillis = millis(); }
	inline void ResetStartingMillis() { this->startingMillis = 0; }
};


//-------------------------------------------------------------------
#endif
//-------------------------------------------------------------------
