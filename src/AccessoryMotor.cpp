/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Class for a motorized accessory>
*************************************************************/

#include "UniversalAccessoryDecoder.h"
#include "AccessoryMotor.hpp"

#ifndef NO_MOTOR

AccessoryMotor::AccessoryMotor(int inDccId, byte inDccIdAccessory, unsigned long inDurationMilli) : Accessory(inDccId, inDccIdAccessory, true, inDurationMilli)
{
}

void AccessoryMotor::Setup(Driver *inpMotor, unsigned char inPort, int inSpeed)
{
	Accessory::Setup(STOP);
	this->pDriver = inpMotor;
	this->port = inPort;
	this->pDriver->SetSpeed(MOTOR_LIGHT, this->port, inSpeed);
	this->prevState = STOP;
	this->type = ACCESSORYMOTOR;
}

void AccessoryMotor::MoveLeft(unsigned long inDuration, int inSpeed)
{
	if (this->IsActionPending())
		return;

#ifdef DEBUG_MODE
	Serial.println(F("AccessoryMotor MoveLeftDir()"));
#endif

	unsigned long duration = inDuration == 0 ? this->GetDuration() : inDuration;
	if (duration > 0)
		StartAction();

	this->pDriver->MoveLeftDir(MOTOR_LIGHT, this->port, -1, inSpeed);
	this->state = LEFT;
	this->prevState = LEFT;
}

void AccessoryMotor::MoveRight(unsigned long inDuration, int inSpeed)
{
	if (this->IsActionPending())
		return;
#ifdef DEBUG_MODE
	Serial.println(F("AccessoryMotor MoveRightDir()"));
#endif
	unsigned long duration = inDuration == 0?this->GetDuration():inDuration;
	if (duration > 0)
		StartAction();

	this->pDriver->MoveRightDir(MOTOR_LIGHT, this->port, -1, inSpeed);
	this->state = RIGHT;
	this->prevState = RIGHT;
}

ACC_STATE AccessoryMotor::MoveToggle(unsigned long inDuration, int inSpeed)
{
	if (this->IsActionPending())
		return this->GetState();

#ifdef DEBUG_MODE
	Serial.print(F("AccessoryMotor MoveToggle() : "));
#endif

	unsigned long duration = inDuration == 0 ? this->GetDuration() : inDuration;
	if (duration > 0)
		StartAction();

	if (this->prevState == LEFT)
	{
		this->pDriver->MoveRightDir(MOTOR_LIGHT, this->port, -1, inSpeed);
		this->state = RIGHT;
	}
	else
	{
		this->pDriver->MoveLeftDir(MOTOR_LIGHT, this->port, -1, inSpeed);
		this->state = LEFT;
	}

	this->prevState = this->state;

	return this->state;
}

void AccessoryMotor::SetState(ACC_STATE inState) 
{ 
	this->state = inState;

	switch(this->state)
	{
		case LEFT:
			this->StartAction();
			this->pDriver->MoveLeftDir(MOTOR_LIGHT, this->port, -1, 0);
			break;

		case RIGHT:
			this->StartAction();
			this->pDriver->MoveRightDir(MOTOR_LIGHT, this->port, -1, 0);
			break;

		default:
			this->pDriver->MoveStop(MOTOR_LIGHT, this->port);
			this->ResetAction();
	}
}

void AccessoryMotor::Move(int inDccId, byte inDccAccessory)
{
	if (this->GetDccPositionSize() == 1)
	{
		this->MoveToggle();
		return;
	}

	int position = this->GetDccPosition(inDccId, inDccAccessory);

	if (position != -1)
		this->SetState((ACC_STATE)position);
}

bool AccessoryMotor::ActionEnded()
{
	bool end = Accessory::ActionEnded();
	if (end)
	{
		this->pDriver->MoveStop(MOTOR_LIGHT, this->port);
		this->state = STOP;
	}

	return end;
}
#endif
