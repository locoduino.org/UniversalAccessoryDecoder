/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Class for a servo accessory>
*************************************************************/

#include "UniversalAccessoryDecoder.h"
#include "AccessoryServo.hpp"

#ifndef NO_SERVO
#include "ActionsStack.hpp"

AccessoryServo::AccessoryServo(int inDccId, byte inDccIdAccessory, unsigned long inDurationMilli) : Accessory(inDccId, inDccIdAccessory, true, inDurationMilli)
{
	this->minimumPosition = 0;
	this->maximumPosition = 180;
	this->powerState = PowerNoAction;
	this->type = ACCESSORYSERVO;
}

AccessoryServo::AccessoryServo(int inDccIdMin, byte inDccIdAccessoryMin, int inDccIdMax, byte inDccIdAccessoryMax, unsigned long inDurationMilli) : Accessory(inDccIdMin, inDccIdAccessoryMin, true, inDurationMilli)
{
	this->minimumPosition = 0;
	this->maximumPosition = 180;
	this->powerState = PowerNoAction;

	this->AddDccPosition(inDccIdMax, inDccIdAccessoryMax, MAXIMUM);
}

void AccessoryServo::Setup(Driver *inpMotor, unsigned char inPort, int inMinimumPosition, int inMaximumPosition, int inDccPositionsNumber)
{
	Accessory::Setup(UNDEFINED);
	this->pDriver = inpMotor;
	this->port = inPort;
	this->minimumPosition = inMinimumPosition;
	this->maximumPosition = inMaximumPosition;
	this->prevState = UNDEFINED;
	this->pDriver->SetupByAccessory(SERVO, this->port, this->minimumPosition);
	this->prevState = MINIMUM;
	this->currentPosition = this->minimumPosition;
	this->targetPosition = -1;
	this->targetSpeed = 0;
	this->powerCommandPin = (GPIO_pin_t) DP_INVALID;
	this->powerDelay = 0;
	this->powerState = PowerNoAction;

	this->AdjustDccPositionsSize(inDccPositionsNumber);
}

void AccessoryServo::SetPowerCommand(int inPin, unsigned long inDelay)
{
	this->powerCommandPin = Arduino_to_GPIO_pin(inPin);

	pinMode2f(this->powerCommandPin, OUTPUT);
	digitalWrite2f(this->powerCommandPin, HIGH);

	this->powerDelay = inDelay;
}

void AccessoryServo::MoveMinimum()
{
#ifdef DEBUG_MODE
	Serial.println(F("AccessoryServo MoveMinimum()"));
#endif

	this->MovePosition(this->minimumPosition);
}

void AccessoryServo::MoveMaximum()
{
#ifdef DEBUG_MODE
	Serial.println(F("AccessoryServo MoveMaximum()"));
#endif

	this->MovePosition(this->maximumPosition);
}

ACC_STATE AccessoryServo::MoveToggle()
{
	if (this->IsActionPending())
		return this->GetState();

#ifdef DEBUG_MODE
	Serial.print(F("AccessoryServo MoveToggle() : "));
#endif

	if (this->prevState == MAXIMUM)
		this->InternalMovePosition(this->minimumPosition);
	else
		this->InternalMovePosition(this->maximumPosition);

	return this->state;
}

void AccessoryServo::SetState(ACC_STATE inState) 
{ 
#ifdef DEBUG_MODE
	Serial.print(F("AccessoryServo SetState("));
#endif

	switch (inState)
	{
		case MINIMUM:
#ifdef DEBUG_MODE
			Serial.println(F("MINIMUM)"));
#endif
			this->InternalMovePosition(this->minimumPosition);
			break;

		case MAXIMUM:
#ifdef DEBUG_MODE
			Serial.println(F("MAXIMUM)"));
#endif
			this->InternalMovePosition(this->maximumPosition);
			break;

		default:
			this->ResetAction();
	}
}

void AccessoryServo::Move(int inDccId, byte inDccAccessory)
{
#ifdef DEBUG_MODE
	Serial.println(F("AccessoryServo Move()"));
#endif

	if (this->GetDccPositionSize() == 1)
	{
		this->MoveToggle();
		return;
	}

	int position = this->GetDccPosition(inDccId, inDccAccessory);

	if (position == MINIMUM || position == MAXIMUM)
		this->SetState((ACC_STATE)position);
	else
		this->MovePosition(position);
}

void AccessoryServo::MovePosition(int inPosition)
{
	if (this->IsActionPending())
		return;

	this->InternalMovePosition(inPosition);
}

void AccessoryServo::InternalMovePosition(int inPosition)
{
	if (this->currentPosition == inPosition)
	{
#ifdef DEBUG_MODE
		Serial.println(F("AccessoryServo::MovePosition : same position - no move!"));
#endif
		return;
	}

#ifdef DEBUG_MODE
	Serial.println(F("AccessoryServo InternalMovePosition()"));
#endif

	MovementSpeed speed = this->GetMovementSpeed();

	switch (speed)
	{
	case ServoFast:
		this->pDriver->MovePosition(SERVO, this->port, 0, inPosition);
		this->currentPosition = inPosition;
		break;

	case ServoAlone:
		for (int pos = this->currentPosition; pos < inPosition; pos++)
			this->pDriver->MovePosition(SERVO, this->port, this->GetDuration(), pos);
		this->currentPosition = inPosition;
		break;

	case ServoActionStack:
		StartAction();
		this->targetPosition = inPosition;
		this->targetSpeed = (this->currentPosition < this->targetPosition) ? 1 : -1;
		ActionsStack::FillingStack = true;
		break;

	case ServoSlow:
		StartAction();
		this->targetPosition = inPosition;
		this->targetSpeed = (this->currentPosition < this->targetPosition) ? 1 : -1;
		break;
	}

	if (inPosition == this->minimumPosition)
	{
		this->state = MINIMUM;
		this->prevState = MINIMUM;
	}
	else
		if (inPosition == this->maximumPosition)
		{
		this->state = MAXIMUM;
		this->prevState = MAXIMUM;
		}
		else
		{
			this->state = UNDEFINED;
			this->prevState = UNDEFINED;
		}
}

bool AccessoryServo::ActionEnded()
{
	if (this->GetActionStartingMillis() <= 0)
		return true;

	if (this->powerCommandPin == DP_INVALID || this->powerState == PowerRunning)
	{
		//if (millis() - this->GetActionStartingMillis() > this->GetDuration())
		{
			if (this->targetSpeed == 0)
			{
				// Abort
				this->targetPosition = -1;
				ActionsStack::FillingStack = false;
				if (this->powerState == PowerRunning && this->powerCommandPin != DP_INVALID)
				{
#ifdef DEBUG_MODE
					Serial.println(F("AccessoryServo after running"));
#endif
					this->powerState = PowerAfterRunning;
					StartAction();
					return false;
				}

				this->ResetAction();
				return true;
			}

			this->currentPosition += this->targetSpeed;
			this->pDriver->MovePosition(SERVO, this->port, this->GetDuration(), this->currentPosition);

			if (this->targetPosition == this->currentPosition)
			{
				this->targetPosition = -1;
				this->targetSpeed = 0;
				ActionsStack::FillingStack = false;
				if (this->powerState == PowerRunning && this->powerCommandPin != DP_INVALID)
				{
#ifdef DEBUG_MODE
					Serial.println(F("AccessoryServo after running"));
#endif
					this->powerState = PowerAfterRunning;
					StartAction();
					return false;
				}
				this->ResetAction();
				return true;
			}
			StartAction();
		}
	}
	else
	{
		if (this->powerState == PowerNoAction)
		{
#ifdef DEBUG_MODE
			Serial.println(F("AccessoryServo start power command"));
#endif
			digitalWrite2f(this->powerCommandPin, LOW);
			this->powerState = PowerBeforeRunning;
			StartAction();
			return false;
		}

		// Action time elapsed, do the next work...
		if (millis() - this->GetActionStartingMillis() > this->powerDelay)
		{
			if (this->powerState == PowerBeforeRunning)
			{
#ifdef DEBUG_MODE
				Serial.println(F("AccessoryServo running"));
#endif
				this->powerState = PowerRunning;
				StartAction();
				return false;
			}

			if (this->powerState == PowerAfterRunning)
			{
#ifdef DEBUG_MODE
				Serial.println(F("AccessoryServo end running"));
#endif
				this->powerState = PowerNoAction;
				digitalWrite2f(this->powerCommandPin, HIGH);
				this->ResetAction();
				return true;
			}
		}
	}

	return false;
}

MovementSpeed AccessoryServo::GetMovementSpeed() const
{
	if (this->GetDuration() == 0)
		return ServoFast;
	if (this->GetDuration() < 6)
		return ServoAlone;
	if (this->GetDuration() < 20)
		return ServoActionStack;
	return ServoSlow;
}

#endif
