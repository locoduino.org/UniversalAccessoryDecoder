//-------------------------------------------------------------------
#ifndef __driver_H__
#define __driver_H__
//-------------------------------------------------------------------

#include "DriverPorts.hpp"

//-------------------------------------------------------------------

#define DRIVER_MAXPORT	10

#define PORT_UNDEFINED	-1

#ifdef DEBUG_MODE
#define CHECKPORT(type, val, text)	CheckPortNb(type, val, F(text))
#define CHECKPIN(val, text)		Driver::CheckPinNb(val, F(text))
#else
#define CHECKPORT(type, val, text)
#define CHECKPIN(val, text)
#endif

#if defined(NO_MOTOR) && defined(NO_LIGHT)
#define NO_MOTOR_LIGHT
#endif

#if defined(NO_MOTOR_LIGHT) && defined(NO_SERVO) && defined(NO_STEPPER)
#error Impossible state : no accessory to drive !
#elif !defined(NO_MOTOR_LIGHT) && !defined(NO_SERVO) && !defined(NO_STEPPER)
enum PORTTYPE
{
	MOTOR_LIGHT = 0,
	SERVO = 1,
	STEPPER = 2
};
#define PORTTYPE_NB	3
#elif defined(NO_MOTOR_LIGHT) && !defined(NO_SERVO) && !defined(NO_STEPPER)
enum PORTTYPE
{
	SERVO = 0,
	STEPPER = 1
};
#define PORTTYPE_NB	2
#elif !defined(NO_MOTOR_LIGHT) && defined(NO_SERVO) && !defined(NO_STEPPER)
enum PORTTYPE
{
	MOTOR_LIGHT = 0,
	STEPPER = 1
};
#define PORTTYPE_NB	2
#elif !defined(NO_MOTOR_LIGHT) && !defined(NO_SERVO) && defined(NO_STEPPER)
enum PORTTYPE
{
	MOTOR_LIGHT = 0,
	SERVO = 1
};
#define PORTTYPE_NB	2
#elif !defined(NO_MOTOR_LIGHT) && defined(NO_SERVO) && defined(NO_STEPPER)
enum PORTTYPE
{
	MOTOR_LIGHT = 0
};
#define PORTTYPE_NB	1
#elif defined(NO_MOTOR_LIGHT) && !defined(NO_SERVO) && defined(NO_STEPPER)
enum PORTTYPE
{
	SERVO = 0
};
#define PORTTYPE_NB	1
#elif defined(NO_MOTOR_LIGHT) && defined(NO_SERVO) && !defined(NO_STEPPER)
enum PORTTYPE
{
	STEPPER = 0
};
#define PORTTYPE_NB	1
#endif

class Driver
{
	protected:
		DriverPorts ports[PORTTYPE_NB];
	
	public:
		Driver(unsigned char inNbPortMotors, unsigned char inNbPortServos, unsigned char inNbPortSteppers);
		
		inline int GetPortsNb(PORTTYPE inType) const { return this->ports[inType].GetSize(); }
		inline PORT_STATE GetState(PORTTYPE inType, int inPort) const { return ports[inType].Get(inPort).GetState(); }
		inline int GetSpeed(PORTTYPE inType, unsigned char inPort) const { return ports[inType].Get(inPort).GetSpeed(); }
		int SetSpeed(PORTTYPE inType, unsigned char inPort, int inSpeed);
		
		inline bool IsLeftDir(PORTTYPE inType, unsigned char inPort) const { return ports[inType].Get(inPort).IsLeftDir(); }
		inline bool IsRightDir(PORTTYPE inType, unsigned char inPort) const { return ports[inType].Get(inPort).IsRightDir(); }
		inline bool IsStopped(PORTTYPE inType, unsigned char inPort) const { return ports[inType].Get(inPort).IsStopped(); }
		
	public:
		virtual void Setup();
		virtual void SetupByAccessory(PORTTYPE inType, unsigned char inPort, int inStartingPosition);

		void MoveLeftDir(PORTTYPE inType, unsigned char inPort, unsigned long inDuration = 0, int inSpeed = 0);
		void MoveRightDir(PORTTYPE inType, unsigned char inPort, unsigned long inDuration = 0, int inSpeed = 0);
		PORT_STATE MoveToggle(PORTTYPE inType, unsigned char inPort, unsigned long inDuration = 0, int inSpeed = 0);
		void MoveStop(PORTTYPE inType, unsigned char inPort);
		void MovePosition(PORTTYPE inType, unsigned char inPort, unsigned long inDuration, int inEndPosition);
		int GetPosition(PORTTYPE inType, unsigned char inPort);

#ifdef DEBUG_MODE
		void CheckPortNb(PORTTYPE inType, unsigned char inPort, const __FlashStringHelper *infunc);
		static void CheckPinNb(int inPin, const __FlashStringHelper *infunc);
		static void CheckPinNb(GPIO_pin_t inPin, const __FlashStringHelper *infunc);
#endif
};


//-------------------------------------------------------------------
#endif
//-------------------------------------------------------------------
