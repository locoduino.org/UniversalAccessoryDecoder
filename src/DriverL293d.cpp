/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Class for a L293D driver>
*************************************************************/

#if !defined(__AVR_ATmega32U4__)
#include "UniversalAccessoryDecoder.h"
#include "DriverL293d.hpp"
#include "DriverPortL293d.hpp"
#include "DriverPortServoArduino.hpp"

#ifndef NO_L293D

DriverL293d::DriverL293d() : Driver(0, 0, 0)
{
#if !defined(NO_MOTOR_LIGHT)
	this->ports[MOTOR_LIGHT].Setup(4);
	this->ports[MOTOR_LIGHT].Add(new DriverPortL293d(1, 200, MOTOR12_1KHZ));	// M1
	this->ports[MOTOR_LIGHT].Add(new DriverPortL293d(2, 200, MOTOR12_1KHZ));	// M2
	this->ports[MOTOR_LIGHT].Add(new DriverPortL293d(3, 200, MOTOR34_1KHZ));	// M3
	this->ports[MOTOR_LIGHT].Add(new DriverPortL293d(4, 200, MOTOR34_1KHZ));	// M4
#endif

#if !defined(NO_SERVO)
	this->ports[SERVO].Setup(2);
	this->ports[SERVO].Add(new DriverPortServoArduino());	// L293D_PORT_SERVO1
	this->ports[SERVO].Add(new DriverPortServoArduino());	// L293D_PORT_SERVO2
#endif

#if !defined(NO_STEPPER)
	this->ports[STEPPER].Setup(3);
	this->ports[STEPPER].Add(new DriverPortStepper2());	// L293D_PORT_STEPPER12
	this->ports[STEPPER].Add(new DriverPortStepper2());	// L293D_PORT_STEPPER34
	this->ports[STEPPER].Add(new DriverPortStepper4());	// L293D_PORT_STEPPER1234
#endif
}

void DriverL293d::Setup()
{
	Driver::Setup();

#if !defined(NO_SERVO)
	((DriverPortServoArduino *) this->ports[SERVO][L293D_PORT_SERVO1])->Setup(SERVO1_PIN, ANALOG);
	((DriverPortServoArduino *) this->ports[SERVO][L293D_PORT_SERVO2])->Setup(SERVO2_PIN, ANALOG);
#endif
}

void DriverL293d::SetupPortMotor(unsigned char inPort, uint8_t inFreq)
{
#if !defined(NO_MOTOR_LIGHT)
	CHECKPORT(MOTOR_LIGHT, inPort, "DriverL293d::SetupPortMotor");
	((DriverPortL293d *) this->ports[MOTOR_LIGHT][inPort])->Setup(inFreq);
#endif
}
#endif
#endif