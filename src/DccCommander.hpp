//-------------------------------------------------------------------
#ifndef __dccCommander_H__
#define __dccCommander_H__
//-------------------------------------------------------------------

#include "UniversalAccessoryDecoder.h"

#ifndef NO_DCC
#ifndef NO_COMMANDER

//-------------------------------------------------------------------

typedef void(*DccBasicAccDecoderPacket)(int address, boolean activate, byte data);

class DccCommander : Commander
{
	public:
		static GPIO_pin_t dccStatusLedPin;
		static boolean UseRawDccAddresses;
		static DccBasicAccDecoderPacket    func_BasicAccPacket;

	public:
		inline DccCommander() : Commander() { this->dccStatusLedPin = DP0; }
		
	public:
		void Setup(int i, int j, int k, boolean inUseRawDccAddresses = false);
		void SetStatusLedPin(int inPin);
		void PriorityLoop();
		bool Loop();
		void SetBasicAccessoryDecoderPacketHandler(DccBasicAccDecoderPacket func);
		static void BaseLoop(int realAddress, byte data);

#ifdef DEBUG_MODE
public:
	void CheckIndex(unsigned char inIndex, const __FlashStringHelper *infunc);
#endif
};

//-------------------------------------------------------------------
#endif
#endif
#endif
//-------------------------------------------------------------------
