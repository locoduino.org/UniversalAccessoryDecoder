/**********************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Class for a light, flashing or not, with optional fading>
***********************************************************************/

#include "UniversalAccessoryDecoder.h"
#include "AccessoryLight.hpp"

#ifndef NO_LIGHT

AccessoryLight::AccessoryLight(int inDccId, byte inDccIdAccessory, unsigned long inBlinkDuration) : Accessory(inDccId, inDccIdAccessory, false, inBlinkDuration)
{
	this->pLight = new AccessoryBaseLight(this);

	this->pLight->SetBlinking(inBlinkDuration);
	this->type = ACCESSORYLIGHT;
}

void AccessoryLight::Move(int inDccId, byte inDccAccessory)
{
	if (this->GetDccPositionSize() == 1)
	{
		this->Toggle();
		return;
	}

	int position = this->GetDccPosition(inDccId, inDccAccessory);

	if (position != -1)
		this->SetState((ACC_STATE)position);
}

#endif
