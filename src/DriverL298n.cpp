/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Class for a L298n driver>
*************************************************************/

#include "UniversalAccessoryDecoder.h"
#include "DriverL298n.hpp"
#include "DriverPortL298n.hpp"

#ifndef NO_L298N

DriverL298n::DriverL298n() : Driver(2, 0, 0)
{
#ifndef NO_MOTOR_LIGHT
	this->ports[MOTOR_LIGHT].Add(new DriverPortL298n());
	this->ports[MOTOR_LIGHT].Add(new DriverPortL298n());
#endif
}

void DriverL298n::SetupPortMotor(unsigned char inPort, int inPinA, int inPinB)
{
#ifndef NO_MOTOR_LIGHT
	CHECKPORT(MOTOR_LIGHT, inPort, "DriverL298n::SetupPortMotor");
	((DriverPortL298n *) this->ports[MOTOR_LIGHT][inPort])->Setup(inPinA, inPinB);
#endif
}

void DriverL298n::SetupPortStepper(unsigned char inPort, int inPinA, int inPinB)
{
#ifndef NO_STEPPER
	CHECKPORT(STEPPER, inPort, "DriverL298n::SetupPortStepper");
	((DriverPortL298n *) this->ports[MOTOR_LIGHT][inPort])->Setup(inPinA, inPinB);
#endif
}

void DriverL298n::Setup()
{
}
#endif
