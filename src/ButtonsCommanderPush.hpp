//-------------------------------------------------------------------
#ifndef __buttonsCommanderPush_H__
#define __buttonsCommanderPush_H__
//-------------------------------------------------------------------

#define  GPIO2_PREFER_SPEED    1
#ifdef UAD_VC
	#include "../VStudio/arduino2.hpp"
#else
	#include "arduino2.hpp"
#endif

#include "ButtonsCommanderButton.hpp"

#define PUSH(list, nb)	((ButtonsCommanderPush *) list[nb])

//-------------------------------------------------------------------
// A push button is a hardware device giving a time limited impulsion.
// It will return one Dccid at a time, passing through the whole list of DccIds.
//-------------------------------------------------------------------

struct DccId
{
	int Decoder;
	byte Accessory;
};

class ButtonsCommanderPush : public ButtonsCommanderButton
{
 private:
	GPIO_pin_t buttonPin;	// the number of the pushbutton pin
	int buttonState;		// the current reading from the input pin
	int lastButtonState;	// the previous reading from the input pin

	unsigned long lastDebounceTime;  // the last time the output pin was toggled
	unsigned long debounceDelay;    // the debounce time; increase if the output flickers
	
	byte dccIdSize;
	byte dccIdAddCounter;
	byte dccIdLoopCounter;
	DccId *pDccId;

 public:
	ButtonsCommanderPush(byte inDccIdNumber);

	void Setup(int inButtonPin);
	void AddDccId(int inDccIdDecoder, byte inDccIdAccessory);
	bool Loop();
	void EndLoop();
	int GetDccIdDecoder() const;
	byte GetDccIdAccessory() const;

	inline int GetPin() const { return GPIO_to_Arduino_pin(this->buttonPin); }
	inline GPIO_pin_t GetPin2() const { return this->buttonPin; }
	inline int GetState() const { return this->buttonState; }
	
	inline bool IsEmpty() const { return this->GetDccIdDecoder() == -1; }
};

//-------------------------------------------------------------------
#endif
//-------------------------------------------------------------------
