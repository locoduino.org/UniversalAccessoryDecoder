/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Class for a group of accessories>
*************************************************************/

#include "UniversalAccessoryDecoder.h"

#ifndef NO_GROUP
#include "ActionsStack.hpp"

#ifdef UAD_VC
#include<stdarg.h>
#endif

#ifdef DEBUG_MODE
#define CHECKINDEX(val, text)	CheckIndex(val, F(text))
#define CHECKINDEXITEM(val, text)	CheckIndex(val, F(text))
#else
#define CHECKINDEX(val, text)
#define CHECKINDEXITEM(val, text)
#endif

/***********************************************************
*		GroupState
************************************************************/

#ifdef DEBUG_MODE
void GroupState::CheckIndex(byte inIndex, const __FlashStringHelper *inFunc)
{
	if (this->size == 0)
	{
		Serial.print(F("Size undefined in "));
		Serial.println(inFunc);
	}
	else
	if (inIndex < 0 || inIndex >= this->size)
	{
		Serial.print(F("Index error in "));
		Serial.print(inFunc);
		Serial.print(F(" : "));
		Serial.print(inIndex, DEC);
		Serial.print(F(" for maximum "));
		Serial.println(this->size, DEC);
	}
}
#endif

GroupState::GroupState(int DccId, byte inDccIdAccessory, bool inSynchrone)
{
	this->DccIdDecoder = DccId;
	this->DccIdAccessory = inDccIdAccessory;
	this->Synchrone = inSynchrone;
	this->pItems = 0;
	this->actionProgressCounter = 255;
	this->addCounter = 0;
}

void GroupState::Setup(byte inSize)
{
#ifdef DEBUG_MODE
	Serial.println(F("   GroupState Setup"));
#endif
	this->size = inSize;
	this->addCounter = 0;
	this->pItems = new GroupStateItem*[inSize];
}

void GroupState::Setup(unsigned char inSize, GroupStateItem *inpFirstItem, ...)
{
	Setup(inSize);

	va_list argList;
	this->Add(inpFirstItem);
	inSize--;

	va_start(argList, inpFirstItem);
	for (; inSize; inSize--)
	{
		GroupStateItem *acc;
		acc = va_arg(argList, GroupStateItem *);
		this->Add(acc);
	}
	va_end(argList);
#ifdef DEBUG_MODE
	if (inSize > 0)
		Serial.println(F("   Not enough items in the list !"));
#endif
}

GroupStateItem* GroupState::operator[](byte inIndex)
{
	CHECKINDEXITEM(inIndex, "GroupState::operator[]");
	return this->pItems[inIndex];
}

void GroupState::Set(byte inIndex, GroupStateItem *inpItem)
{
	CHECKINDEXITEM(inIndex, "GroupState::Set");

	this->pItems[inIndex] = inpItem;
}

// Returns the index of the new added accessory.
unsigned char GroupState::Add(GroupStateItem *inpItem)
{
	CHECKINDEXITEM(this->addCounter, "GroupState::Add");

#ifdef DEBUG_MODE
	Serial.print(F("Add group state item: "));
	Serial.print((int) inpItem->pAccessory, DEC);
	Serial.print(F("  state: "));
	Serial.println(inpItem->State);
#endif

	this->pItems[this->addCounter++] = inpItem;
	return this->addCounter - 1;
}

unsigned char GroupState::Add(Accessory *inpAccessory, ACC_STATE inState) 
{ 
	return Add(new GroupStateItem(inpAccessory, inState)); 
}

void GroupState::StartAction()
{
	if (IsActionPending())
	{
		this->actionProgressCounter = 255;
		return;
	}

	this->startActionTime = 0;

	if (this->Synchrone)
	{
		for (byte i = 0; i < this->size; i++)
			this->pItems[i]->pAccessory->StartAction(this->pItems[i]->State);

		this->actionProgressCounter = 255;
		return;
	}

	this->actionProgressCounter = 0;
	if (this->pItems[this->actionProgressCounter]->Delay != 0)
		this->startActionTime = millis();

	CHECKINDEXITEM(this->actionProgressCounter, "GroupState::StartAction");
	this->pItems[this->actionProgressCounter]->pAccessory->StartAction(this->pItems[this->actionProgressCounter]->State);
}

void GroupState::Loop()
{
	if (!IsActionPending())
		return;

	if (this->pItems[this->actionProgressCounter]->pAccessory->IsGroupActionPending())
		return;

	if (this->startActionTime != 0)
		if (millis() - this->startActionTime < this->pItems[this->actionProgressCounter]->Delay)
			return;

#ifdef DEBUG_MODE
	Serial.print(F("AccessoryGroupState next action state accessory "));
	Serial.println(this->actionProgressCounter);
#endif

	++this->actionProgressCounter;

	if (this->actionProgressCounter >= this->size)
	{
		this->actionProgressCounter = 255;
		return;
	}

	CHECKINDEXITEM(this->actionProgressCounter, "GroupState::Loop"); 
	this->pItems[this->actionProgressCounter]->pAccessory->StartAction(this->pItems[this->actionProgressCounter]->State);
}

/***********************************************************
*		AccessoryGroup
************************************************************/

#ifdef DEBUG_MODE
void AccessoryGroup::CheckIndex(byte inIndex, const __FlashStringHelper *inFunc)
{
	if (this->size == 0)
	{
		Serial.print(F("Size undefined in "));
		Serial.println(inFunc);
	}
	else
	if (inIndex < 0 || inIndex >= this->size)
	{
		Serial.print(F("Index error in "));
		Serial.println(inFunc);
	}
}
#endif

StaticGroupData AccessoryGroup::StaticData;

StaticGroupData::StaticGroupData()
{
	this->AccessoryGroupSize = 4;
	this->pAccessoryGroupFullList = new AccessoryGroup*[AccessoryGroupSize];
	this->AccessoryGroupAddCounter = 0;
}

AccessoryGroup::AccessoryGroup()
{
	this->size = 0;
	this->pStates = 0;
	this->addCounter = 0;
	this->CurrentState = -1;
}

void AccessoryGroup::Setup(byte num)
{
#ifdef DEBUG_MODE
	Serial.println(F("   AccessoryGroup Setup"));
#endif
	this->size = num;
	this->addCounter = 0;

	this->pStates = new GroupState*[num];
	AddAccessoryGroup(this);
}

void AccessoryGroup::Setup(byte num, GroupState *inpFirstState, ...)
{
#ifdef DEBUG_MODE
	Serial.println(F("   AccessoryGroup Setup"));
#endif
	va_list argList;

	this->size = num;
	this->addCounter = 0;
	this->pStates = new GroupState*[num];
	this->Add(inpFirstState);
	num--;

	va_start(argList, inpFirstState);
	for (; num; num--)
	{
		GroupState *acc;
		acc = va_arg(argList, GroupState *);
		this->Add(acc);
	}
	va_end(argList);
#ifdef DEBUG_MODE
	if (num > 0)
		Serial.println(F("   Not enough items in the list !"));
#endif

	AddAccessoryGroup(this);
}

void AccessoryGroup::AddAccessoryGroup(AccessoryGroup *inGroup)
{
	if (StaticData.AccessoryGroupAddCounter == StaticData.AccessoryGroupSize)
	{
		AccessoryGroup **pNewList = new AccessoryGroup*[StaticData.AccessoryGroupSize + 1];

		for (int i = 0; i < StaticData.AccessoryGroupSize; i++)
			pNewList[i] = StaticData.pAccessoryGroupFullList[i];

		StaticData.AccessoryGroupSize++;
		delete StaticData.pAccessoryGroupFullList;
		StaticData.pAccessoryGroupFullList = pNewList;
	}

	StaticData.pAccessoryGroupFullList[StaticData.AccessoryGroupAddCounter++] = inGroup;
}

GroupState* AccessoryGroup::operator[](byte inIndex)
{
	CHECKINDEX(inIndex, "AccessoryGroup::operator[]");
	return this->pStates[inIndex];
}

void AccessoryGroup::Set(byte inIndex, GroupState *inpState)
{
	CHECKINDEX(inIndex, "AccessoryGroup::Set");
	this->pStates[inIndex] = inpState;
}

// Returns the index of the new added accessory.
byte AccessoryGroup::Add(GroupState *inpState)
{
	CHECKINDEX(this->addCounter, "AccessoryGroup::Add");

#ifdef DEBUG_MODE
	Serial.print(F("Add group state: "));
	Serial.print(inpState->DccIdDecoder);
	Serial.print(F("/"));
	Serial.println(inpState->DccIdAccessory);
#endif

	this->pStates[this->addCounter++] = inpState;
	return this->addCounter - 1;
}

void AccessoryGroup::AddRange(const AccessoryGroup &inGroup)
{
	GroupState **pNewList = new GroupState*[this->size + inGroup.size];

	int i = 0;
	for (; i < this->size; i++)
		pNewList[i] = this->pStates[i];

	for (; i < this->size + inGroup.size; i++)
		pNewList[i] = inGroup.pStates[i];

	this->size = this->size + inGroup.size;
	delete this->pStates;
	this->pStates = pNewList;
}

GroupState *AccessoryGroup::GetByID(int inDccId, byte inDccIdAccessory)
{
	int acc = IndexOf(inDccId, inDccIdAccessory);
	if (acc == -1)
		return 0;

	return this->pStates[acc];
}

int AccessoryGroup::IndexOf(GroupState *inpState)
{
	for (byte i = 0; i < this->size; i++)
		if (this->pStates[i] == inpState)
			return i;

	return -1;
}

int AccessoryGroup::IndexOf(int inDccId, byte inDccIdAccessory)
{
	for (byte i = 0; i < this->size; i++)
		if (this->pStates[i]->DccIdDecoder == inDccId && this->pStates[i]->DccIdAccessory == inDccIdAccessory)
			return i;
	return -1;
}

bool AccessoryGroup::IsActionPending()
{
	return (this->CurrentState >= 0 && this->CurrentState < this->size);
}

void AccessoryGroup::StartAction(byte inState)
{
	CHECKINDEX(inState, "AccessoryGroup::StartAction");

#ifdef DEBUG_MODE
	Serial.print(F("AccessoryGroup start action state "));
	Serial.println(inState);
#endif

	this->CurrentState = inState;
	this->pStates[inState]->StartAction();
}

bool AccessoryGroup::Loop()
{
#ifndef NO_COMMANDER
	Commander::StaticData.CommanderPriorityLoop();
#endif

	if (!IsActionPending())
		return false;	// nothing done !

	this->pStates[this->CurrentState]->Loop();
	if (!this->pStates[this->CurrentState]->IsActionPending())
		this->CurrentState = -1;
	return true;
}

void AccessoryGroup::ButtonToggle(int inButton)
{
	if (ActionsStack::FillingStack)
	{
		ActionsStack::Actions.Add(inButton);
		return;
	}

#ifdef DEBUG_MODE
	Serial.print(F("AccessoryGroup "));
	Serial.println(F(" moved by its button !"));
#endif
	if(!this->IsActionPending())
		this->CurrentState = 0;
	else
	{
		this->CurrentState++;
		if (this->CurrentState >= this->size)
			this->CurrentState = 0;
	}
	this->StartAction(this->CurrentState);
}

bool AccessoryGroup::DCCToggle(int inDccId, byte inDccIdAccessory)
{
	int newState = this->IndexOf(inDccId, inDccIdAccessory);

	if (newState == -1)
		return false;

	if (ActionsStack::FillingStack)
	{
		ActionsStack::Actions.Add(inDccId, inDccIdAccessory);
		return true;
	}

#ifdef DEBUG_MODE
	Serial.print(F("AccessoryGroup moved by DCC "));
	Serial.print(inDccId);
	Serial.print(F(" / "));
	Serial.println(inDccIdAccessory);
#endif
	if (!this->IsActionPending())
	{
#ifdef DEBUG_MODE
		Serial.println(F("No action pending..."));
#endif
		if (newState != -1 && newState != this->CurrentState)
		{
			this->StartAction(newState);
		}
	}

	return true;
}
#endif
