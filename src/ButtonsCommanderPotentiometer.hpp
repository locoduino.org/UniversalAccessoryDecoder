//-------------------------------------------------------------------
#ifndef __potentiometer_H__
#define __potentiometer_H__
//-------------------------------------------------------------------

#ifdef UAD_VC
#include "../VStudio/arduino2.hpp"
#else
#include "arduino2.hpp"
#endif
#include "ButtonsCommanderButton.hpp"

//-------------------------------------------------------------------

#define POTENTIOMETER(list, nb)	((ButtonsCommanderPotentiometer *) list[nb])

class ButtonsCommanderPotentiometer : public ButtonsCommanderButton
{
 private:
	int currentValue;
	int pin;
	int moveAccuracy;
	int mini, maxi;
	
 public:
	ButtonsCommanderPotentiometer(int inDccIdDecoder, byte inDccIdAccessory, int inMinimum, int inMaximum);

	inline bool IsAnalog() const { return true; }
	inline int GetPosition() const { return this->currentValue; }

	void Setup(int inPin, int inMoveAccuracy = 1);
	bool Loop();
};

//-------------------------------------------------------------------
#endif
//-------------------------------------------------------------------
