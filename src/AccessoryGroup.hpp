//-------------------------------------------------------------------
#ifndef __accessoryGroup_H__
#define __accessoryGroup_H__
//-------------------------------------------------------------------

#include "Accessory.hpp"

//-------------------------------------------------------------------

#ifndef NO_GROUP

// One item : Motor1/left or Light18 -/On
// The delay gives a duration of the movement, to ensure that this movement is finished before starting the next one. Only used in asynchronous group state.
class GroupStateItem
{
	public:
		Accessory *pAccessory;
		ACC_STATE State;
		unsigned int Delay;

		GroupStateItem(Accessory *inpAccessory, ACC_STATE inState, unsigned int inDelay = 0) { this->pAccessory = inpAccessory; this->State = inState; this->Delay = inDelay; }
};

// A complete state of the group : Light18/On and Motor3/Right .
class GroupState
{
	public:
		int DccIdDecoder;
		byte DccIdAccessory;
		GroupStateItem* *pItems;
		bool Synchrone;

	private:
		byte size;
		byte addCounter;
		byte actionProgressCounter;
		unsigned long startActionTime;

	public:
		GroupState(int DccId, byte inDccIdAccessory, bool inSynchrone = true);

		void Setup(byte inSize, GroupStateItem *inpFirstItem, ...);
		void Setup(byte inSize);
		void Set(byte inIndex, GroupStateItem *inpItem);
		GroupStateItem *operator[](unsigned char idx);		
	
		void StartAction();
		void ResetAction()	{ this->actionProgressCounter = 255; }
		bool IsActionPending()	{ return this->actionProgressCounter < 255; }

		void Loop();

#ifdef DEBUG_MODE
		void CheckIndex(byte inIndex, const __FlashStringHelper *infunc);
#endif
	public:
		unsigned char Add(GroupStateItem *inpItem);
		unsigned char Add(Accessory *inpAccessory, ACC_STATE inState);
};

// Describes static data only intialized once at the beginning of execution.

class AccessoryGroup;

class StaticGroupData
{
public:
	// List of pointers	to all groups declared.
	// The commanders can enumerate this list to find thing to move...
	AccessoryGroup* *pAccessoryGroupFullList;
	byte AccessoryGroupSize;
	byte AccessoryGroupAddCounter;

	StaticGroupData();
};

// Group of accessories defining a list of states of these acessories.
class AccessoryGroup
{
	public:
		static StaticGroupData StaticData;

	private:
		byte size;
		byte addCounter;

	public:
		GroupState* *pStates;
		int CurrentState;	// Must be -1 !

	public:
		AccessoryGroup();

		void Setup(byte inSize);
		void Setup(byte inSize, GroupState *inpFirstState, ...);
		void Set(byte inIndex, GroupState *inpState);
		void AddRange(const AccessoryGroup &inGroup);
		GroupState *operator[](byte idx);
		GroupState *GetByID(int inDccId, byte inDccIdAccessory);
		int IndexOf(GroupState *inpState);
		int IndexOf(int inDccId, byte inDccIdAccessory);
		static void AddAccessoryGroup(AccessoryGroup *inGroup);

		void StartAction(unsigned char inStateIndex);
		void ResetAction()	{ this->CurrentState = -1; }
		bool IsActionPending();

		bool Loop();
		void ButtonToggle(int inButton);
		bool DCCToggle(int inDccId, byte inDccIdAccessory);

#ifdef DEBUG_MODE
		void CheckIndex(byte inIndex, const __FlashStringHelper *infunc);
#endif
	public:
		byte Add(GroupState *inpState);
};
#endif


//-------------------------------------------------------------------
#endif
//-------------------------------------------------------------------
