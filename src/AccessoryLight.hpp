//-------------------------------------------------------------------
#ifndef __accessoryLight_H__
#define __accessoryLight_H__
//-------------------------------------------------------------------

#include "Accessory.hpp"
#include "AccessoryBaseLight.hpp"

//-------------------------------------------------------------------

#ifndef NO_LIGHT

#define LIGHTON		STATE_FIRST
#define LIGHTBLINK	STATE_SECOND
#define LIGHTOFF	STATE_NONE

// This class describes a single light accessory.

#define LIGHT(list, nb)	((AccessoryLight *) (list)[nb])

class AccessoryLight : public Accessory
{
	private:
		Driver *pDriver;
		AccessoryBaseLight *pLight;

	public:
		AccessoryLight(int inDccId, byte inDccIdAccessory, unsigned long inBlinkDuration = 0);
		inline void Setup(Driver *inpDriver, unsigned char inPort, int inIntensity = 255) { this->pLight->Setup(inpDriver, inPort, inIntensity); }
		inline void SetFading(byte inStep, byte inDelay) { this->pLight->SetFading(inStep, inDelay); }

		inline bool IsOn() const { return this->pLight->IsOn(); }
		inline bool IsFlashing() const { return this->pLight->IsBlinking(); }
		inline bool IsFading() const { return this->pLight->IsFading(); }
		inline Driver *GetDriver() const { return this->pDriver; }
		inline unsigned char GetPort() const { return this->pLight->GetPort(); }

		inline void SetState(ACC_STATE inState) { this->pLight->SetState(inState); }
		void Move(int inDccId, byte inDccAccessory);
		inline ACC_STATE Toggle() { return this->pLight->Toggle(); }
		inline void LightOn() { this->pLight->LightOn(); }
		inline void LightOff() { this->pLight->LightOff(); }
		inline void Blink() { this->pLight->Blink(); }

		inline bool IsGroupActionPending() { return this->pLight->IsGroupActionPending(); }
		inline void StartAction() { this->pLight->StartAction(); }
		inline bool ActionEnded() { return this->pLight->ActionEnded(); }
		inline void ResetAction() { return this->ResetStartingMillis(); }
};
#endif


//-------------------------------------------------------------------
#endif
//-------------------------------------------------------------------
