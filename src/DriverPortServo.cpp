/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Driver port for a servo>
*************************************************************/

#include "UniversalAccessoryDecoder.h"

#ifndef NO_SERVO

DriverPortServo::DriverPortServo()
{
}

void DriverPortServo::Setup()
{
}

void DriverPortServo::SetupByAccessory(int inStartingPosition)
{
}

void DriverPortServo::MovePosition(unsigned long inDuration, int inEndPosition)
{
}

int DriverPortServo::GetPosition()
{
	return 0;
}
#endif