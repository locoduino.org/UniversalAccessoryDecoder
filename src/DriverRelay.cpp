/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Class for a relay driver>
*************************************************************/

#include "UniversalAccessoryDecoder.h"

#ifndef NO_RELAY
#ifndef NO_MOTOR_LIGHT

// Only height for maxi size, but it can be increased if you have memory space...

DriverRelay::DriverRelay(unsigned char inPortsNb) : Driver(inPortsNb, 0, 0)
{
	for(unsigned char i = 0; i < inPortsNb; i++)
		this->ports[MOTOR_LIGHT].Add(new DriverPortRelay());
}

void DriverRelay::SetupPort(unsigned char inPort, int inPin)
{
	CHECKPORT(MOTOR_LIGHT, inPort, "DriverRelay::SetupPort");
	((DriverPortRelay *) this->ports[MOTOR_LIGHT][inPort])->Setup(inPin);
}

#endif
#endif