//-------------------------------------------------------------------
#ifndef __driverPortL293d_H__
#define __driverPortL293d_H__
//-------------------------------------------------------------------

#if !defined(__AVR_ATmega32U4__)
#if defined UAD_VC
#define RELEASE		1
#define FORWARD		2
#define BACKWARD	3
class AF_DCMotor
{
public:
	AF_DCMotor(int, int) {}

	void run(int) {}
	void setSpeed(uint8_t) {}

	int pwmfreq;
};
#else
#include "AFMotor.hpp"
#endif
#include "DriverPort.hpp"

//-------------------------------------------------------------------

//-------------------------------------------------------------------

class DriverPortL293d : public DriverPort
{
	protected:
		AF_DCMotor *pmotor;

	public:
		DriverPortL293d(unsigned char inOutPort, uint8_t inSpeed, uint8_t inFreq);
		
		void Setup(uint8_t inFreq);
		
		int SetSpeed(uint8_t inSpeed);
		
		void MoveLeftDir(unsigned long inDuration = 0);
		void MoveRightDir(unsigned long inDuration = 0);
		void MoveStop();
};


//-------------------------------------------------------------------
#endif
#endif
//-------------------------------------------------------------------
