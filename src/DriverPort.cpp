/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Driver port>
*************************************************************/

#include "UniversalAccessoryDecoder.h"

DriverPort::DriverPort()
{
	this->state = PORT_STOP;
	this->speed = DEFAULTSPEED;
}

int DriverPort::SetSpeed(int inSpeed)
{
	uint8_t oldSpeed = this->speed;
	this->speed = inSpeed;
	return oldSpeed;
}

PORT_STATE DriverPort::MoveToggle(unsigned long inDuration)
{
	if (this->IsLeftDir())
	{
		MoveRightDir(inDuration);
	}
	else
	{
		MoveLeftDir(inDuration);
	}
		
	return this->GetState();
}
