//-------------------------------------------------------------------
#ifndef __accessories_H__
#define __accessories_H__
//-------------------------------------------------------------------

#include "Accessory.hpp"

//-------------------------------------------------------------------

//-------------------------------------------------------------------

// Describes static data only intialized once at the beginning of execution.

class Accessories;

class StaticAccessoriesData
{
public:
	// List of pointers	to all accessories lists declared.
	// The commanders can enumerate this list to find thing to move...
	Accessories* *pAccessoriesFullList;
	byte AccessoriesSize;
	byte AccessoriesAddCounter;

	StaticAccessoriesData();
};

class Accessories
{
	public:
		static StaticAccessoriesData AccessoriesFullList;

	private:
		byte size;
		byte addCounter;
		Accessory* *pList;
		
	protected:
	
	public:
		Accessories();
		
		Accessory *GetById(int inDccId, byte inDccIdAccessory) const;
		byte GetSize() const { return this->size; }
		void Setup(byte num);
		void Setup(byte num, Accessory *pFirstAccessory, ...);
		static void AddAccessoriesList(Accessories *inAccessories);
		void Set(unsigned char inIndex, Accessory *inpAccessory);
		unsigned char Add(Accessory *inpAccessory);
		void AddRange(const Accessories &inAccessories);
		Accessory *operator[](unsigned char idx);

		bool Loop();

		bool CanMove(int inDccId, byte inDccIdAccessory);

		void ButtonToggle(int inIndex);
		bool DCCToggle(int DccId, byte inDccIdAccessory);
		bool MovePosition(int DccId, byte inDccIdAccessory, int inPosition);
		bool IsActionPending();

	public:
#ifdef DEBUG_MODE
		void CheckIndex(byte inIndex, const __FlashStringHelper *infunc);
#endif
};

//-------------------------------------------------------------------
#endif
//-------------------------------------------------------------------
