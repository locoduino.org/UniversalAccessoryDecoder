/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Class for a generic accessory>
*************************************************************/

#include "UniversalAccessoryDecoder.h"

#ifdef DEBUG_MODE
void Accessory::CheckDccId(int inDccIdDecoder, byte inDccIdAccessory, const __FlashStringHelper *inFunc)
{
	if (inDccIdDecoder < 0 || inDccIdDecoder >= 9999)
	{
		Serial.print(F("Dcc ID Decoder "));
		Serial.print(inDccIdDecoder);
		Serial.print(F(" error in "));
		Serial.println(inFunc);
	}

	if (inDccIdAccessory < 0 || inDccIdAccessory > 1)
	{
		Serial.print(F("Dcc ID Accessory "));
		Serial.print(inDccIdAccessory);
		Serial.print(F(" error in "));
		Serial.println(inFunc);
	}
}
#endif

Accessory::Accessory(int inDccId, byte inDccIdAccessory, bool inUseStateNone, unsigned long inDuration)
{
#ifdef DEBUG_MODE
	CHECKDCC(inDccId, inDccIdAccessory, "Accessory constructor");
#endif

	this->dccPositionsSize = 0;
	this->dccPositionsAddCounter = 0;
	this->pDccPositions = 0;
	this->lastDccPosition.DccId = -1;
	this->lastDccPosition.DccIdAccessory = -1;
	this->debounceDelay = 300;
	this->SetLastMoveTime();

	this->AddDccPosition(inDccId, inDccIdAccessory, STATE_FIRST);

	this->duration = inDuration;
	this->startingMillis = 0;
	this->useStateNone = inUseStateNone;
	this->type = ACCESSORYUNDEFINED;
}

void Accessory::Setup(ACC_STATE inStartingState)
{
	this->state = inStartingState;
}

void Accessory::ClearDccPositions()
{
	if (0 >= this->dccPositionsSize)
		return;

	if (this->pDccPositions != 0)
		delete[] this->pDccPositions;

	this->pDccPositions = 0;
	this->dccPositionsSize = 0;
}

void Accessory::AdjustDccPositionsSize(int inNewSize)
{
	if (inNewSize <= this->dccPositionsSize)
		return;

	DccPosition *pNewList = new DccPosition[inNewSize];
	int i = 0;
	for (; i < this->dccPositionsSize; i++)
		pNewList[i] = this->pDccPositions[i];

	for (; i < inNewSize; i++)
		pNewList[i].DccId = -1;	//empty

	this->dccPositionsSize = inNewSize;
	if (this->pDccPositions != 0)
		delete[] this->pDccPositions;
	this->pDccPositions = pNewList;
}

// Returns the index of the new added position.
unsigned char Accessory::AddDccPosition(int inDccId, byte inDccIdAccessory, int inPosition)
{
	Accessory::AdjustDccPositionsSize(this->dccPositionsAddCounter + 1);

	this->pDccPositions[this->dccPositionsAddCounter].DccId = inDccId;
	this->pDccPositions[this->dccPositionsAddCounter].DccIdAccessory = inDccIdAccessory;
	this->pDccPositions[this->dccPositionsAddCounter++].Position = inPosition;

	return this->dccPositionsAddCounter - 1;
}

int Accessory::GetDccPositionIndex(int inDccId, byte inDccAccessory)
{
	for (int i = 0; i < this->dccPositionsSize; i++)
		if (this->pDccPositions[i].DccId == inDccId && this->pDccPositions[i].DccIdAccessory == inDccAccessory)
			return i;

	return -1;
}

int Accessory::GetDccPosition(int inDccId, byte inDccAccessory)
{
	int i = this->GetDccPositionIndex(inDccId, inDccAccessory);
	if (i != -1)
		return this->pDccPositions[i].Position;
	return -1;
}

void Accessory::StartAction()
{
	if (this->duration > 0)
		this->startingMillis = millis();

#ifdef DEBUG_MODE
#ifdef DEBUG_VERBOSE_MODE
	Serial.print(F("Accessory start action "));
	Serial.println(this->startingMillis);
#endif
#endif
}

void Accessory::StartAction(ACC_STATE inState)
{
	if (this->duration > 0)
		this->startingMillis = millis();
	this->SetState(inState);

#ifdef DEBUG_MODE
#ifdef DEBUG_VERBOSE_MODE
	Serial.print(F("Accessory start action at "));
	Serial.print(this->startingMillis);
	Serial.print(F("ms for state "));
	Serial.print(inState);

	if (this->startingMillis == 0)
		Serial.println(" ended");
	else
		Serial.println("");
#endif
#endif
}

void Accessory::ResetAction()
{
#ifdef DEBUG_MODE
#ifdef DEBUG_VERBOSE_MODE
	Serial.print(F("End (reset) action at "));
	Serial.print(millis() - this->startingMillis);
	Serial.print(F("ms for "));
	Serial.print(this->duration);
	Serial.println(F("ms"));
#endif
#endif

	this->startingMillis = 0;
}

bool Accessory::Loop()
{
	return this->ActionEnded();
}

bool Accessory::ActionEnded()
{
	if (this->startingMillis <= 0)
		return false;

	if ((unsigned long)(millis() - this->startingMillis) > this->duration)
	{
#ifdef DEBUG_MODE
#ifdef DEBUG_VERBOSE_MODE
		Serial.print(F("End action at "));
		Serial.print(millis() - this->startingMillis);
		Serial.print(F("ms for "));
		Serial.print(this->duration);
		Serial.println(F("ms"));
#endif
#endif
		this->startingMillis = 0;
		return true;
	}

	return false;
}


