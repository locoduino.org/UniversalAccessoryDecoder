/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Basic push button with debounce.>
*************************************************************/

#include "UniversalAccessoryDecoder.h"
#include "ButtonsCommanderPush.hpp"

ButtonsCommanderPush::ButtonsCommanderPush(byte inDccIdNumber) : ButtonsCommanderButton(-1, -1)
{
	this->buttonPin = (GPIO_pin_t)DP_INVALID;
	this->lastButtonState = LOW;
	this->dccIdSize = inDccIdNumber;
	this->pDccId = new DccId[this->dccIdSize];
	this->dccIdAddCounter = 0;
	this->dccIdLoopCounter = 0;

	this->lastDebounceTime = 0;
	this->debounceDelay = 50;
}

void ButtonsCommanderPush::Setup(int inButtonPin)
{	
	CHECKPIN(inButtonPin, "ButtonsCommanderPush::Setup");

	this->buttonPin = Arduino_to_GPIO_pin(inButtonPin);

	pinMode2f(this->buttonPin, INPUT_PULLUP);
}

// Returns the index of the new added position.
void ButtonsCommanderPush::AddDccId(int inDccId, byte inDccIdAccessory)
{
#ifdef DEBUG_MODE
	Accessory::CHECKDCC(inDccId, inDccIdAccessory, "Accessory constructor");
#endif

#ifdef DEBUG_MODE
	if (this->dccIdAddCounter == this->dccIdSize)
	{
		Serial.println(F("ButtonsCommanderPush::AddDccId : Too many DccId for this push button !"));
		return;
	}
#endif

	this->pDccId[this->dccIdAddCounter].Decoder = inDccId;
	this->pDccId[this->dccIdAddCounter++].Accessory = inDccIdAccessory;
}

int ButtonsCommanderPush::GetDccIdDecoder() const 
{ 
	return this->pDccId[this->dccIdLoopCounter].Decoder;
}

byte ButtonsCommanderPush::GetDccIdAccessory() const
{ 
	return this->pDccId[this->dccIdLoopCounter].Accessory;
}

bool ButtonsCommanderPush::Loop()
{
	if (this->buttonPin == DP_INVALID)
		return false;

	bool haveChanged = false;
	
	// read the state of the switch into a local variable:
	int reading = digitalRead2f(this->buttonPin);

	// check to see if you just pressed the button 
	// (i.e. the input went from LOW to HIGH),  and you've waited 
	// long enough since the last press to ignore any noise:  

	// If the switch changed, due to noise or pressing:
	if (reading != this->lastButtonState)
	{
		// reset the debouncing timer
		this->lastDebounceTime = millis();
	}

	if (this->lastDebounceTime > 0 && (millis() - this->lastDebounceTime) > this->debounceDelay)
	{
		// whatever the reading is at, it's been there for longer
		// than the debounce delay, so take it as the actual current state:

		// if the button state has changed:
		if (reading != this->buttonState)
		{
			this->buttonState = reading;

			// only toggle the state if the new button state is HIGH
			if (this->buttonState == LOW)
				haveChanged = true;
		}
		this->lastDebounceTime = 0;    
	}
  
	// save the reading.  Next time through the loop,
	// it'll be the lastButtonState:
	lastButtonState = reading;
	return haveChanged;
}

void ButtonsCommanderPush::EndLoop()
{
	this->dccIdLoopCounter++;
	if (this->dccIdLoopCounter >= this->dccIdAddCounter)
		this->dccIdLoopCounter = 0;
}
