/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Generic driver>
*************************************************************/

#include "UniversalAccessoryDecoder.h"

#ifdef DEBUG_MODE
void Driver::CheckPortNb(PORTTYPE inType, unsigned char inPort, const __FlashStringHelper *inFunc)
{
	if (inPort < 0 || inPort >= this->GetPortsNb(inType))
	{
		Serial.print(F("Port "));
		Serial.print(inPort);
		Serial.print(F(" error in "));
		Serial.println(inFunc);
	}
}
#endif

#ifdef DEBUG_MODE
void Driver::CheckPinNb(GPIO_pin_t inPin, const __FlashStringHelper *inFunc)
{
	return CheckPinNb(GPIO_to_Arduino_pin(inPin), inFunc);
}

void Driver::CheckPinNb(int inPin, const __FlashStringHelper *inFunc)
{
	if (inPin <= 0 || inPin >= NUM_DIGITAL_PINS)
	{
		Serial.print(F("Pin "));
		Serial.print(inPin);
		Serial.print(F(" error in "));
		Serial.println(inFunc);
	}
}
#endif

Driver::Driver(unsigned char inNbPortMotors, unsigned char inNbPortServos, unsigned char inNbPortSteppers)
{
#if !defined(NO_MOTOR_LIGHT)
	this->ports[MOTOR_LIGHT].Setup(inNbPortMotors);
#endif
#if !defined(NO_SERVO)
	this->ports[SERVO].Setup(inNbPortServos);
#endif
#if !defined(NO_STEPPER)
	this->ports[STEPPER].Setup(inNbPortSteppers);
#endif
}

void Driver::Setup()
{
}

void Driver::SetupByAccessory(PORTTYPE inType, unsigned char inPort, int inStartingPosition)
{
	CHECKPORT(inType, inPort, "Driver::SetupByAccessory");
	this->ports[inType][inPort]->SetupByAccessory(inStartingPosition);
}

int Driver::SetSpeed(PORTTYPE inType, unsigned char inPort, int inSpeed)
{
	CHECKPORT(inType, inPort, "Driver::SetSpeed");
	return this->ports[inType][inPort]->SetSpeed(inSpeed);
}

void Driver::MoveLeftDir(PORTTYPE inType, unsigned char inPort, unsigned long inDuration, int inSpeed)
{
	CHECKPORT(inType, inPort, "Driver::MoveLeftDir");
	int oldSpeed = -1;
	if (inSpeed >= 0)
		oldSpeed = this->ports[inType][inPort]->SetSpeed(inSpeed);
	this->ports[inType][inPort]->MoveLeftDir(inDuration);
	if (oldSpeed != -1)
		this->ports[inType][inPort]->SetSpeed(oldSpeed);
}

void Driver::MoveRightDir(PORTTYPE inType, unsigned char inPort, unsigned long inDuration, int inSpeed)
{
	CHECKPORT(inType, inPort, "Driver::MoveRightDir");
	int oldSpeed = -1;
	if (inSpeed >= 0)
		oldSpeed = this->ports[inType][inPort]->SetSpeed(inSpeed);
	this->ports[inType][inPort]->MoveRightDir(inDuration);
	if (oldSpeed != -1)
		this->ports[inType][inPort]->SetSpeed(oldSpeed);
}

PORT_STATE Driver::MoveToggle(PORTTYPE inType, unsigned char inPort, unsigned long inDuration, int inSpeed)
{
	CHECKPORT(inType, inPort, "Driver::MoveToggle");
	int oldSpeed = -1;
	if (inSpeed > 0)
		oldSpeed = this->ports[inType][inPort]->SetSpeed(inSpeed);
	PORT_STATE ret = this->ports[inType][inPort]->MoveToggle(inDuration);
	if (oldSpeed != -1)
		this->ports[inType][inPort]->SetSpeed(oldSpeed);
	return ret;
}

void Driver::MoveStop(PORTTYPE inType, unsigned char inPort)
{
	CHECKPORT(inType, inPort, "Driver::MoveToggle");
	this->ports[inType][inPort]->MoveStop();
}

void Driver::MovePosition(PORTTYPE inType, unsigned char inPort, unsigned long inDuration, int inPosition)
{
	CHECKPORT(inType, inPort, "Driver::MovePosition");
	this->ports[inType][inPort]->MovePosition(inDuration, inPosition);
}

int Driver::GetPosition(PORTTYPE inType, unsigned char inPort)
{
	CHECKPORT(inType, inPort, "Driver::GetPosition");
	return this->ports[inType][inPort]->GetPosition();
}

