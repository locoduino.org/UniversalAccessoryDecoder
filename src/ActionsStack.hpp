//-------------------------------------------------------------------
#ifndef __actionsStack_H__
#define __actionsStack_H__
//-------------------------------------------------------------------

#include "Arduino.h"
#include "UniversalAccessoryDecoder.h"

//-------------------------------------------------------------------

// An action is what can be done by the user to ask for a accessory movement.
// It can be a Dcc call, a button pushed... Other kind of actions can be added here if necessary.

class Action
{
	public:
		int DccCode;
		int DccCodeAccessory;
		int PushedButton;

		Action(int inDccCode, byte inDccCodeAccessory, int PushedButton);
};

// This is a FIFO stack, grabbing all user actions to be done later. When the stack reach the size,
// other actions are lost...

class ActionsStack
{
	private:
		unsigned char size;
		unsigned char addCounter;
		Action* *pList;
		
	public:
		ActionsStack(int inSize);
		
		unsigned char Add(int inButton);
		unsigned char Add(int inDccCode, byte inDccCodeAccessory);
		Action *operator[](unsigned char idx);
		void Purge();
		void Purge(int inIndex);
		Action *GetActionToExecute();
		int GetNumber() const;

		static ActionsStack Actions;
		static bool FillingStack;

	private:
		void CheckIndex(unsigned char inIndex, const __FlashStringHelper *infunc) const;
};


//-------------------------------------------------------------------
#endif
//-------------------------------------------------------------------
