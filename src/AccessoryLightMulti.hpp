//-------------------------------------------------------------------
#ifndef __accessoryLightMulti_H__
#define __accessoryLightMulti_H__
//-------------------------------------------------------------------

#include "Accessory.hpp"
#include "AccessoryBaseLight.hpp"
#include "Driver.hpp"

//-------------------------------------------------------------------

#ifndef NO_LIGHT

// This class describes a basic light.

#define LIGHTMULTI(list, nb)	((AccessoryLightMulti *) (list)[nb])

class AccessoryLightMulti : public Accessory
{
	private:
		AccessoryBaseLight *pLights;
		int *pDccPositionBlinks;
		byte lightsSize;

	public:
		AccessoryLightMulti(int inDccId, byte inDccIdAccessory, byte inSize, unsigned long inBlinkDuration);
		
		inline bool IsOn(byte inIndex) const { return this->pLights[inIndex].IsOn(); }
		inline bool IsFlashing(byte inIndex) const { return this->pLights[inIndex].IsBlinking(); }
		inline bool IsFading(byte inIndex) const { return this->pLights[inIndex].IsFading(); }
		inline Driver *GetDriver(byte inIndex) const { return this->pLights[inIndex].GetDriver(); }
		inline unsigned char GetPort(byte inIndex) const { return this->pLights[inIndex].GetPort(); }
		inline byte GetSize() const { return this->lightsSize; }

		unsigned char AddDccPosition(int inDccIdMin, byte inDccIdAccessoryMin, int inOnMask, int inBlinkMask);

		inline void SetState(byte inIndex, ACC_STATE inState) { this->pLights[inIndex].SetState(inState); }
		inline ACC_STATE Toggle(byte inIndex) { return this->pLights[inIndex].Toggle(); }
		inline void SetBlinking(byte inIndex, unsigned long inBlinkingDelay) { this->pLights[inIndex].SetBlinking(inBlinkingDelay); }
		inline void SetFading(byte inIndex, byte inStep, byte inDelay) { this->pLights[inIndex].SetFading(inStep, inDelay); }
		void Setup();
		void SetupLight(byte inIndex, Driver *inpDriver, unsigned char inPort, int inIntensity = 255);

		inline void LightOn(byte inIndex) { this->pLights[inIndex].LightOn(); }
		inline void LightOff(byte inIndex) { this->pLights[inIndex].LightOff(); }
		inline void Blink(byte inIndex) { this->pLights[inIndex].Blink(); }
		inline bool IsGroupActionPending(byte inIndex) { return this->pLights[inIndex].IsGroupActionPending(); }
		inline void StartAction(byte inIndex) { this->pLights[inIndex].StartAction(); }
		inline bool ActionEnded(byte inIndex) { return this->pLights[inIndex].ActionEnded(); }

		void LightOn();
		void LightOff();
		void Blink();

		bool ActionEnded();

		ACC_STATE Toggle();
		void Move(int inDccId, byte inDccAccessory);
		void Move(int position);
		void MoveBlink(int inOnMask, int inBlinkMask);
#ifdef DEBUG_MODE
		void CheckIndex(int inIndex, const __FlashStringHelper *infunc);
#endif
};
#endif


//-------------------------------------------------------------------
#endif
//-------------------------------------------------------------------
