/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Class for a one way motorized accessory>
*************************************************************/

#include "UniversalAccessoryDecoder.h"
#include "AccessoryMotorOneWay.hpp"

#ifndef NO_MOTOR
#ifndef NO_MOTORONEWAY

AccessoryMotorOneWay::AccessoryMotorOneWay(int inDccId, byte inDccIdAccessory, unsigned long inDurationMilli) : AccessoryMotor(inDccId, inDccIdAccessory, inDurationMilli)
{
}

#endif
#endif
