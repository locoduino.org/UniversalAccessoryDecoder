//-------------------------------------------------------------------
#ifndef __accessoryMotor_H__
#define __accessoryMotor_H__
//-------------------------------------------------------------------

#include "Accessory.hpp"
#include "Driver.hpp"


//-------------------------------------------------------------------

#ifndef NO_MOTOR

// This class describes a motor powered by a driver.

#define LEFT	STATE_FIRST
#define RIGHT	STATE_SECOND
#define STOP	STATE_NONE

enum DIRECTION { ONLYLEFT, ONLYRIGHT, BOTHDIRS };

class AccessoryMotor : public Accessory
{
	protected:
		Driver *pDriver;
		unsigned char port;
	
	public:
		AccessoryMotor(int inDccId, byte inDccIdAccessory, unsigned long inDurationMilli = 0);
		inline unsigned char GetPort() const { return this->port; }
		bool ActionEnded();
		
		inline bool IsLeft() const { return this->IsFirst(); }
		inline bool IsRight() const { return this->IsSecond(); }
		inline bool IsStopped() const { return this->IsNone(); }
		
	public:
		void SetState(ACC_STATE instate);
		virtual void Setup(Driver *inpDriver, unsigned char inPort, int inSpeed);
		void Move(int inDccId, byte inDccAccessory);
		virtual void MoveLeft(unsigned long inDuration = 0, int inSpeed = 0);
		virtual void MoveRight(unsigned long inDuration = 0, int inSpeed = 0);
		virtual ACC_STATE MoveToggle(unsigned long inDuration = 0, int inSpeed = 0);
		inline ACC_STATE Toggle() { return MoveToggle(); }
};
#endif


//-------------------------------------------------------------------
#endif
//-------------------------------------------------------------------

