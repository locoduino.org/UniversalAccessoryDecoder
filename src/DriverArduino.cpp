/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Class for the Arduino itself, as a driver>
*************************************************************/

#include "UniversalAccessoryDecoder.h"

#ifndef NO_ARDUINODRIVER

// Only height for maxi size, but it can be increased if you have memory space...

DriverArduino::DriverArduino(unsigned char inPortMotorsNb, unsigned char inPortServosNb) : Driver(inPortMotorsNb, inPortServosNb, 0)
{
#if !defined(NO_MOTOR_LIGHT)
	for (int i = 0; i < inPortMotorsNb; i++)
		this->ports[MOTOR_LIGHT].Add(new DriverPortArduino());
#endif
#if !defined(NO_SERVO)
	for (int i = 0; i < inPortServosNb; i++)
		this->ports[SERVO].Add(new DriverPortServoArduino());
#endif
}

void DriverArduino::SetupPortMotor(unsigned char inPort, int inPin, PORT_TYPE inType)
{
	CHECKPORT(MOTOR_LIGHT, inPort, "DriverArduino::SetupPortMotor");
	((DriverPortArduino *) this->ports[MOTOR_LIGHT][inPort])->Setup(inPin, inType);
}

void DriverArduino::SetupPortServo(unsigned char inPort, int inPin, PORT_TYPE inType)
{
#if !defined(NO_SERVO)
	CHECKPORT(SERVO, inPort, "DriverArduino::SetupPortServo");
	((DriverPortServoArduino *) this->ports[SERVO][inPort])->Setup(inPin, inType);
#endif
}


#endif
