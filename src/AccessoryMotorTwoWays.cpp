/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Class for a two ways motorized accessory>
*************************************************************/

#include "UniversalAccessoryDecoder.h"
#include "AccessoryMotorTwoWays.hpp"

#ifndef NO_MOTOR
#ifndef NO_MOTORTWOWAYS

AccessoryMotorTwoWays::AccessoryMotorTwoWays(int inDccId, byte inDccIdAccessory, unsigned long inDurationMilli) : AccessoryMotor(inDccId, inDccIdAccessory, inDurationMilli)
{
}

AccessoryMotorTwoWays::AccessoryMotorTwoWays(int inDccIdLeft, byte inDccIdAccessoryLeft, int inDccIdRight, byte inDccIdAccessoryRight, unsigned long inDurationMilli) : AccessoryMotor(inDccIdLeft, inDccIdAccessoryLeft, inDurationMilli)
{
	this->AddDccPosition(inDccIdRight, inDccIdAccessoryRight, RIGHT);
}

#endif
#endif
