/**********************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Class for a light, flashing or not, with optional fading>
***********************************************************************/

#include "UniversalAccessoryDecoder.h"
#include "AccessoryLightMulti.hpp"

#ifndef NO_LIGHT

#ifdef DEBUG_MODE
#define CHECK(val, text)	CheckIndex(val, F(text))
#else
#define CHECK(val, text)
#endif

#ifdef DEBUG_MODE
void AccessoryLightMulti::CheckIndex(int inIndex, const __FlashStringHelper *inFunc)
{
	if (this->lightsSize == 0)
	{
		Serial.print(F("Size undefined in "));
		Serial.println(inFunc);
	}
	else
		if (inIndex < 0 || inIndex >= this->lightsSize)
		{
			Serial.print(F("Index error in "));
			Serial.println(inFunc);
		}
}
#endif

AccessoryLightMulti::AccessoryLightMulti(int inDccId, byte inDccIdAccessory, byte inSize, unsigned long inBlinkDuration) : Accessory(inDccId, inDccIdAccessory, false, inBlinkDuration)
{
	this->lightsSize = inSize;

	this->pLights = new AccessoryBaseLight[inSize];

	for (byte i = 0; i < inSize; i++)
	{
		this->pLights[i].pOwner = this;
		this->pLights[i].SetBlinking(inBlinkDuration);
	}
	this->type = ACCESSORYLIGHTMULTI;

	this->pDccPositionBlinks = 0;
}

void AccessoryLightMulti::Setup()
{
}

unsigned char AccessoryLightMulti::AddDccPosition(int inDccIdMin, byte inDccIdAccessoryMin, int inOnMask, int inBlinkMask)
{
#ifdef DEBUG_MODE
	Serial.print(F("AccessoryBaseLight: AddDccPosition "));
	Serial.print(inDccIdMin);
	Serial.print("/");
	Serial.print(inDccIdAccessoryMin);
	Serial.print(": ");
	Serial.print(inOnMask, BIN);
	Serial.print(" / ");
	Serial.println(inBlinkMask, BIN);
#endif
	unsigned char pos = Accessory::AddDccPosition(inDccIdMin, inDccIdAccessoryMin, inOnMask);

	if (this->pDccPositionBlinks == 0)
	{
		this->pDccPositionBlinks = new int[this->GetDccPositionSize()];
		for (int i = 0; i < this->GetDccPositionSize(); i++)
			this->pDccPositionBlinks[i] = 0;
	}

	this->pDccPositionBlinks[pos] = inBlinkMask;
	return 0;
}

void AccessoryLightMulti::SetupLight(byte inIndex, Driver *inpDriver, unsigned char inPort, int inIntensity)
{
	CHECK(inIndex, "SetupLight");
	this->pLights[inIndex].Setup(inpDriver, inPort, inIntensity, this);
	this->LightOff(inIndex);
}

void AccessoryLightMulti::LightOn()
{
	for (byte i = 0; i < this->lightsSize; i++)
		this->LightOn(i);
}

void AccessoryLightMulti::LightOff()
{
	for (byte i = 0; i < this->lightsSize; i++)
		this->LightOff(i);
}

void AccessoryLightMulti::Blink()
{
	for (byte i = 0; i < this->lightsSize; i++)
		this->Blink(i);
}

bool AccessoryLightMulti::ActionEnded()
{
	bool res = false;
	for (byte i = 0; i < this->lightsSize; i++)
		res |= this->ActionEnded(i);

	return res;
}

ACC_STATE AccessoryLightMulti::Toggle()
{
	ACC_STATE localState;

	for (byte i = 0; i < this->lightsSize; i++)
		localState = this->Toggle(i);

	return localState;
}

void AccessoryLightMulti::Move(int inPosition)
{
	if (inPosition != -1)
	{
		for (byte i = 0; i < this->lightsSize; i++)
			if (inPosition & (1 << i))
				this->SetState(i, LIGHTON);
			else
				this->SetState(i, LIGHTOFF);
	}
}

void AccessoryLightMulti::MoveBlink(int inOnMask, int inBlinkMask)
{
	if (inOnMask != -1)
	{
		for (byte i = 0; i < this->lightsSize; i++)
		{
			this->SetBlinking(i, 0);
			if (inOnMask & (1 << i))
			{
				if (inBlinkMask & (1 << i))
				{
					this->SetBlinking(i, this->GetDuration());
					this->SetState(i, LIGHTBLINK);
				}
				else
					this->SetState(i, LIGHTON);
			}
			else
				this->SetState(i, LIGHTOFF);
		}
	}
}

void AccessoryLightMulti::Move(int inDccId, byte inDccAccessory)
{
	int position = this->GetDccPosition(inDccId, inDccAccessory);

	if (position != -1)
	{
		int positionIndex = this->GetDccPositionIndex(inDccId, inDccAccessory);
		if (this->pDccPositionBlinks != 0)
			MoveBlink(position, this->pDccPositionBlinks[positionIndex]);
		else
			Move(position);
	}
	else
	{
		for (byte i = 0; i < this->lightsSize; i++)
			this->Toggle(i);
	}
}

#endif
