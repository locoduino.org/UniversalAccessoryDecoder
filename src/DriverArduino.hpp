//-------------------------------------------------------------------
#ifndef __driverArduino_H__
#define __driverArduino_H__
//-------------------------------------------------------------------

#include "Driver.hpp"

//-------------------------------------------------------------------

//-------------------------------------------------------------------
#ifndef NO_ARDUINODRIVER
class DriverArduino : public Driver
{
	private:
	
	public:
		DriverArduino(unsigned char inPortMotorsNb, unsigned char inPortServosNb);

	public:
		void Setup() {}
		void SetupPortMotor(unsigned char inPort, int inPin, PORT_TYPE inType = DIGITAL);
		void SetupPortServo(unsigned char inPort, int inPin, PORT_TYPE inType = ANALOG);
};
#endif



//-------------------------------------------------------------------
#endif
//-------------------------------------------------------------------
