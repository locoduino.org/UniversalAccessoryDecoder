//-------------------------------------------------------------------
#ifndef __buttonsCommanderButton_H__
#define __buttonsCommanderButton_H__
//-------------------------------------------------------------------

#include "Arduino.h"

//-------------------------------------------------------------------
// A button is a hardware device allowing user to activate an accessory or a group.
// when this device is used, a DCC id is sent, and the according accessory
// can do the job.
//-------------------------------------------------------------------

class ButtonsCommanderButton
{
 private:
	 int DccIdDecoder;
	 byte DccIdAccessory;

 public:
	 inline ButtonsCommanderButton(int inDccIdDecoder, byte inDccIdAccessory) { this->DccIdDecoder = inDccIdDecoder; this->DccIdAccessory = inDccIdAccessory; }
	 inline virtual int GetDccIdDecoder() const { return this->DccIdDecoder; }
	 inline virtual byte GetDccIdAccessory() const { return this->DccIdAccessory; }
	 inline virtual bool IsAnalog() const { return false; }
	 inline virtual int GetPosition() const { return 0; }

	 inline virtual bool Loop() { return false; }
	 inline virtual void EndLoop() {}
};

//-------------------------------------------------------------------
#endif
//-------------------------------------------------------------------
