/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Switch button with debounce.>
*************************************************************/

#include "UniversalAccessoryDecoder.h"
#include "ButtonsCommanderSwitch.hpp"

ButtonsCommanderSwitch::ButtonsCommanderSwitch(byte inDccIdNumber) : ButtonsCommanderButton(-1, -1)
{
	this->dccIdSize = inDccIdNumber;
	this->pDccId = new DccIdPin[this->dccIdSize];
	this->dccIdAddCounter = 0;
	this->dccIdLoopCounter = 0;

	this->debounceDelay = 50;
}

void ButtonsCommanderSwitch::Setup()
{
	for (byte i = 0; i < this->dccIdAddCounter; i++)
	{
		DccIdPin *id = &(this->pDccId[i]);
		if (id->Pin == DP_INVALID)
			continue;

		pinMode2f(id->Pin, INPUT_PULLUP);
	}
}

// Returns the index of the new added position.
void ButtonsCommanderSwitch::AddDccId(int inDccId, byte inDccIdAccessory, int inPin)
{
#ifdef DEBUG_MODE
	Accessory::CHECKDCC(inDccId, inDccIdAccessory, "Accessory constructor");
#endif

	CHECKPIN(inPin, "ButtonsCommanderSwitch::AddDccId");

#ifdef DEBUG_MODE
	if (this->dccIdAddCounter == this->dccIdSize)
	{
		Serial.println(F("ButtonsCommanderSwitch::AddDccId : Too many DccId for this switch !"));
		return;
	}
#endif

	this->pDccId[this->dccIdAddCounter].Decoder = inDccId;
	this->pDccId[this->dccIdAddCounter].Accessory = inDccIdAccessory;
	this->pDccId[this->dccIdAddCounter].Pin = Arduino_to_GPIO_pin(inPin);
	this->pDccId[this->dccIdAddCounter].buttonState = 0;
	this->pDccId[this->dccIdAddCounter].lastButtonState = LOW;
	this->pDccId[this->dccIdAddCounter++].lastDebounceTime = 0;

	pinMode2f(this->pDccId[this->dccIdAddCounter].Pin, INPUT_PULLUP);
}

int ButtonsCommanderSwitch::GetDccIdDecoder() const
{
	return this->pDccId[this->dccIdState].Decoder;
}

byte ButtonsCommanderSwitch::GetDccIdAccessory() const
{
	return this->pDccId[this->dccIdState].Accessory;
}

bool ButtonsCommanderSwitch::Loop()
{
	if (this->dccIdLoopCounter >= this->dccIdAddCounter)
		this->dccIdLoopCounter = 0;

	DccIdPin *id = &(this->pDccId[this->dccIdLoopCounter++]);
	if (id->Pin == DP_INVALID)
		return false;

	// read the state of the switch into a local variable:
	int reading = digitalRead2f(id->Pin);

	// check to see if you just pressed the button 
	// (i.e. the input went from LOW to HIGH),  and you've waited 
	// long enough since the last press to ignore any noise:  

	// If the switch changed, due to noise or pressing:
	if (reading != id->lastButtonState)
	{
		// reset the debouncing timer
		id->lastDebounceTime = millis();
	}

	bool haveFound = false;

	if (id->lastDebounceTime > 0 && (millis() - id->lastDebounceTime) > this->debounceDelay)
	{
		// whatever the reading is at, it's been there for longer
		// than the debounce delay, so take it as the actual current state:

		// if the button state has changed:
		if (reading != id->buttonState)
		{
			id->buttonState = reading;

			// only toggle the state if the new button state is HIGH
			if (id->buttonState == HIGH)
			{
				this->dccIdState = this->dccIdLoopCounter-1;
				haveFound = true;
			}
		}
		id->lastDebounceTime = 0;
	}

	// save the reading.  Next time through the loop,
	// it'll be the lastButtonState:
	id->lastButtonState = reading;

	return haveFound;
}
