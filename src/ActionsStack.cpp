/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Class for a actions stack>
*************************************************************/

#include "ActionsStack.hpp"

#ifdef DEBUG_MODE
#define CHECK(val, text)	CheckIndex(val, F(text))
#else
#define CHECK(val, text)
#endif

Action::Action(int inDccCode, byte inDccCodeAccessory, int inPushedButton)
{
	this->DccCode = inDccCode;
	this->DccCodeAccessory = DccCodeAccessory;
	this->PushedButton = inPushedButton;
}

ActionsStack ActionsStack::Actions(ACTION_STACK_SIZE);
bool ActionsStack::FillingStack(false);

#ifdef DEBUG_MODE
void ActionsStack::CheckIndex(unsigned char inIndex, const __FlashStringHelper *inFunc)	const
{
	if (this->size == 0)
	{
		Serial.print(F("Size undefined in "));
		Serial.println(inFunc);
	}
	else
	if (inIndex < 0 || inIndex >= this->size)
	{
		Serial.print(F("Index error in "));
		Serial.println(inFunc);
	}
}
#endif

ActionsStack::ActionsStack(int inSize)
{
	this->size = inSize;
	this->addCounter = 0;
	this->pList = new Action*[inSize];

	for (int i = 0; i < this->size; i++)
	{
		this->pList[i] = 0;
	}
}

// Returns the index of the new added action.
unsigned char ActionsStack::Add(int inButton)
{
	if (addCounter >= size)
		return size + 1;	// action lost, the stack is full !

	CHECK(addCounter, "ActionsStack::Add Button");
	this->pList[addCounter++] = new Action(-1, -1, inButton);

	return addCounter - 1;
}

// Returns the index of the new added action.
unsigned char ActionsStack::Add(int inDccCode, byte inDccCodeAccessory)
{
	if (addCounter >= size)
		return size + 1;	// action lost, the stack is full !

	CHECK(addCounter, "ActionsStack::Add Dcc");
	this->pList[addCounter++] = new Action(inDccCode, inDccCodeAccessory, -1);

	return addCounter - 1;
}

Action *ActionsStack::GetActionToExecute()
{
	for (int i = 0; i < this->size; i++)
	{
		if (this->pList[i] != 0)
		{
			Action *ret = this->pList[i];
			this->pList[i] = 0;
			return ret;
		}
	}

	return 0;
}

// Returns the index of the new added action.
void ActionsStack::Purge(int inIndex)
{
	if (this->pList[inIndex] != 0)
	{
		delete this->pList[inIndex];
		this->pList[inIndex] = 0;
	}
}

// Returns the index of the new added action.
void ActionsStack::Purge()
{
	this->addCounter = 0;

	for (int i = 0; i < this->size; i++)
	{
		delete this->pList[i];
		this->pList[i] = 0;
	}
}

// Returns the number of stacked actions.
int ActionsStack::GetNumber() const
{
	int count = 0;

	for (int i = 0; i < this->size; i++)
	{
		if (this->pList[i] != 0)
			count++;
	}

	return count;
}

Action *ActionsStack::operator[](unsigned char inIndex)
{
	CHECK(inIndex, "ActionsStack::operator[]");
	return this->pList[inIndex];
}
