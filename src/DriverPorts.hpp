//-------------------------------------------------------------------
#ifndef __driverPorts_H__
#define __driverPorts_H__
//-------------------------------------------------------------------

#include "DriverPort.hpp"

//-------------------------------------------------------------------

#ifdef DEBUG_MODE
#define CHECKPORTINDEX(val, text)	CheckIndex(val, F(text))
#else
#define CHECKPORTINDEX(val, text)
#endif

class DriverPorts
{
	private:
		unsigned char size;
		unsigned char addCounter;
		DriverPort **pList;
		
	protected:
	
	public:
		DriverPorts();
		
		void Setup(unsigned char inSize);
		void Set(unsigned char inIndex, DriverPort *pDriverPort);
		unsigned char Add(DriverPort *pDriverPort);
		DriverPort *operator[](unsigned char idx);
		inline const DriverPort &Get(unsigned char idx) const { return *pList[idx]; }
		inline int GetSize() const { return this->size; }

	public:
	
#ifdef DEBUG_MODE
		void CheckIndex(unsigned char inIndex, const __FlashStringHelper *infunc);
#endif
};


//-------------------------------------------------------------------
#endif
//-------------------------------------------------------------------
