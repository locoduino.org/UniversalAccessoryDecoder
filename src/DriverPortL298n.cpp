/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Driver port for L298n>
*************************************************************/

#include "UniversalAccessoryDecoder.h"
#include "DriverPortL298n.hpp"

#ifndef NO_L298N

DriverPortL298n::DriverPortL298n()
{
}

void DriverPortL298n::Setup(int inPinA, int inPinB)
{
	this->pinA = Arduino_to_GPIO_pin(inPinA);
	this->pinB = Arduino_to_GPIO_pin(inPinB);

	CHECKPIN(this->pinA, "DriverPortL298n::Setup");
	CHECKPIN(this->pinB, "DriverPortL298n::Setup");

	pinMode2f(this->pinA, OUTPUT);
	pinMode2f(this->pinB, OUTPUT);
}

uint8_t DriverPortL298n::SetSpeed(uint8_t inSpeed)
{
	return 0;
}

void DriverPortL298n::MoveLeftDir(unsigned long inDuration)
{
	CHECKPIN(this->pinA, "DriverPortL298n::MoveLeftDir");
	CHECKPIN(this->pinB, "DriverPortL298n::MoveLeftDir");
#ifdef DEBUG_MODE
	Serial.print(this->pinA);
	Serial.print(F(" / "));
	Serial.print(this->pinB);
	Serial.print(F(" DriverPortL298n MoveLeftDir() "));
	if (inDuration != -1)
	{
		Serial.print(F("for "));
		Serial.print(inDuration);
		Serial.println(F("ms"));
	}
	else
		Serial.println("");
#endif

	digitalWrite2f(this->pinA, HIGH);
	digitalWrite2f(this->pinB, LOW);

	if (inDuration != -1)
	{
		delay(inDuration);

		digitalWrite2f(this->pinA, LOW);
		digitalWrite2f(this->pinB, LOW);
	}

	this->state = PORT_LEFT;
}

void DriverPortL298n::MoveRightDir(unsigned long inDuration)
{
	CHECKPIN(this->pinA, "DriverPortL298n::MoveRightDir");
	CHECKPIN(this->pinB, "DriverPortL298n::MoveRightDir");
#ifdef DEBUG_MODE
	Serial.print(this->pinA);
	Serial.print(F(" / "));
	Serial.print(this->pinB);
	Serial.print(F(" DriverPortL298n MoveRightDir() "));
	if (inDuration != -1)
	{
		Serial.print(F("for "));
		Serial.print(inDuration);
		Serial.println(F("ms"));
	}
	else
		Serial.println("");
#endif

	digitalWrite2f(this->pinA, LOW);
	digitalWrite2f(this->pinB, HIGH);

	if (inDuration != -1)
	{
		delay(inDuration);

		digitalWrite2f(this->pinA, LOW);
		digitalWrite2f(this->pinB, LOW);
	}

	this->state = PORT_RIGHT;
}

void DriverPortL298n::MoveStop()
{
	CHECKPIN(this->pinA, "DriverPortL298n::MoveStop");
	CHECKPIN(this->pinB, "DriverPortL298n::MoveStop");

	digitalWrite2f(this->pinA, LOW);
	digitalWrite2f(this->pinB, LOW);

	this->state = PORT_STOP;
}
#endif
