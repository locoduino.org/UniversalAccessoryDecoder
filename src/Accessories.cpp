/*************************************************************
project: <Universal Accessory Decoder>
author: <Thierry PARIS>
description: <Class for an accessory list>
*************************************************************/

#include "UniversalAccessoryDecoder.h"
#include "ActionsStack.hpp"

#ifdef DEBUG_MODE
#define CHECK(val, text)	CheckIndex(val, F(text))
#else
#define CHECK(val, text)
#endif

#ifdef UAD_VC
#include<stdarg.h>
#endif

#ifdef DEBUG_MODE
void Accessories::CheckIndex(byte inIndex, const __FlashStringHelper *inFunc)
{
	if (this->size == 0)
	{
		Serial.print(F("Size undefined in "));
		Serial.println(inFunc);
	}
	else
		if (inIndex < 0 || inIndex >= this->size)
		{
			Serial.print(F("Index error in "));
			Serial.println(inFunc);
		}
}
#endif

StaticAccessoriesData Accessories::AccessoriesFullList;

StaticAccessoriesData::StaticAccessoriesData()
{
	this->AccessoriesSize = 4;
	this->pAccessoriesFullList = new Accessories*[this->AccessoriesSize];
	this->AccessoriesAddCounter = 0;
}

Accessories::Accessories()
{
	this->size = 0;
	this->pList = 0;
	this->addCounter = 0;
}

void Accessories::Setup(byte num)
{
	this->size = num;
	this->addCounter = 0;
	this->pList = new Accessory*[num];

	AddAccessoriesList(this);
}

void Accessories::Setup(byte num, Accessory *inpFirstAccessory, ...)
{
	va_list argList;

	this->size = num;
	this->addCounter = 0;
	this->pList = new Accessory*[num];

	this->Add(inpFirstAccessory);
	num--;

	va_start(argList, inpFirstAccessory);
	for (; num; num--)
	{
		Accessory *acc;
		acc = va_arg ( argList, Accessory * );
		this->Add(acc);
	}
	va_end(argList);
#ifdef DEBUG_MODE
	if (num > 0)
		Serial.println(F("   Not enough items in the list !"));
#endif

	AddAccessoriesList(this);
}

void Accessories::AddAccessoriesList(Accessories *inAccessories)
{
	if (AccessoriesFullList.AccessoriesAddCounter == AccessoriesFullList.AccessoriesSize)
	{
		Accessories **pNewList = new Accessories*[AccessoriesFullList.AccessoriesSize + 1];

		for (int i = 0; i < AccessoriesFullList.AccessoriesSize; i++)
			pNewList[i] = AccessoriesFullList.pAccessoriesFullList[i];

		AccessoriesFullList.AccessoriesSize++;
		delete AccessoriesFullList.pAccessoriesFullList;
		AccessoriesFullList.pAccessoriesFullList = pNewList;
	}

	AccessoriesFullList.pAccessoriesFullList[AccessoriesFullList.AccessoriesAddCounter++] = inAccessories;
}

void Accessories::Set(unsigned char inIndex, Accessory *pAcc)
{
	CHECK(inIndex, "Accessories::Set");
	this->pList[inIndex] = pAcc;
}

// Returns the index of the new added accessory.
unsigned char Accessories::Add(Accessory *pAcc)
{
	CHECK(addCounter, "Accessories::Add");
	this->pList[addCounter++] = pAcc;

	return addCounter - 1;
}

void Accessories::AddRange(const Accessories &inAccessories)
{
	Accessory **pNewList = new Accessory*[this->size + inAccessories.size];

	int i = 0;
	for (; i < this->size; i++)
		pNewList[i] = this->pList[i];

	for (; i < this->size + inAccessories.size; i++)
		pNewList[i] = inAccessories.pList[i];

	this->size = this->size + inAccessories.size;
	delete this->pList;
	this->pList = pNewList;
}

Accessory *Accessories::GetById(int inDccId, byte inDccIdAccessory) const
{
	for (unsigned char i = 0; i < this->size; i++)
	{
		if (this->pList[i]->GetDccPosition(inDccId, inDccIdAccessory) != -1)
			return this->pList[i];
	}

	return 0;
}

Accessory *Accessories::operator[](unsigned char inIndex)
{
	CHECK(inIndex, "Accessories::operator[]");
	return this->pList[inIndex];
}

bool Accessories::IsActionPending()
{
	for (unsigned char i = 0; i < this->size; i++)
	{
		if (this->pList[i]->IsActionPending())
		{
			return true;
		}
	}

	return false;
}

bool Accessories::CanMove(int inDccId, byte inDccIdAccessory)
{
	Accessory *acc = this->GetById(inDccId, inDccIdAccessory);

	if (acc == 0)
		return false;

	// Move if there is more than one DccPosition (no toggle id), and dcc ids are different than
	// previous time...
	if (acc->GetDccPositionSize() > 1)
	{
		bool move = acc->GetLastDccPosition().DccId != inDccId || acc->GetLastDccPosition().DccIdAccessory != inDccIdAccessory;
#ifdef DEBUG_MODE
		if (!move)
			Serial.println(F("Same position : Cant move !"));
#endif
		return move;
	}

	if (millis() - acc->GetLastMoveTime() <= acc->GetDebounceDelay())
	{
#ifdef DEBUG_MODE
	#ifdef DEBUG_VERBOSE_MODE
		Serial.print(F("Debounce : Cant move DccId "));
		Serial.print(inDccId);
		Serial.print(F(" / "));
		Serial.print(inDccIdAccessory);
		Serial.println(F(" : not enough time since last move !"));
	#endif
#endif
		return false;
	}

	acc->SetLastMoveTime();
	return true;
}

static int loopIndix = 0;

bool Accessories::Loop()
{
#ifndef NO_COMMANDER
	Commander::StaticData.CommanderPriorityLoop();
#endif

	if (loopIndix >= this->size)
		loopIndix = 0;

	this->pList[loopIndix++]->Loop();

	// Look for action stack pending...
	if (this->IsActionPending())
		return false;

	Action *act = ActionsStack::Actions.GetActionToExecute();
	if (act == 0)
		return false;	// nothing done !

	if (this->CanMove(act->DccCode, act->DccCodeAccessory))
	{
		if (act->DccCode >= 0)
		{
			DCCToggle(act->DccCode, act->DccCodeAccessory);
			return true;
		}
		else
		{
			if (act->PushedButton < this->size)
			{
				ButtonToggle(act->PushedButton);
				return true;
			}
		}
	}

	return false;
}

void Accessories::ButtonToggle(int inButton)
{
	if (ActionsStack::FillingStack)
	{
#ifdef DEBUG_MODE
		Serial.print(F(" ---- Stack button nb "));
		Serial.println(inButton);
#endif
		ActionsStack::Actions.Add(inButton);
		return;
	}

#ifdef DEBUG_MODE
	Serial.print(F("Accessory nb "));
	Serial.print(inButton);
	Serial.println(F(" moved by its button !"));
#endif

	Accessory *acc = (*this)[inButton];

	acc->Toggle();
}

bool Accessories::DCCToggle(int inDccId, byte inDccIdAccessory)
{
	if (ActionsStack::FillingStack)
	{
#ifdef DEBUG_MODE
		Serial.print(F(" ---- Stack Dcc id "));
		Serial.println(inDccId);
#endif
		ActionsStack::Actions.Add(inDccId, inDccIdAccessory);
		return false;
	}

	Accessory *acc = this->GetById(inDccId, inDccIdAccessory);

	if (acc == 0)
	{
		return false;
	}

	if (!this->CanMove(inDccId, inDccIdAccessory))
	{
		return true;
	}

	acc->SetLastDccPosition(inDccId, inDccIdAccessory);

#ifdef DEBUG_MODE
	for (int i = 0; i < this->size; i++)
		if (acc == (*this)[i])
		{
			Serial.print(F("DCCToggle : Accessory "));
			Serial.print(i);
			Serial.print(F(" DccId "));
			Serial.print(inDccId);
			Serial.print(F(" / "));
			Serial.println(inDccIdAccessory);
			break;
		}
#endif

	acc->Move(inDccId, inDccIdAccessory);

	return true;
}

bool Accessories::MovePosition(int inDccId, byte inDccIdAccessory, int inPosition)
{
	/*
	if (ActionsStack::FillingStack)
	{
#ifdef DEBUG_MODE
		Serial.print(F(" ---- Stack Dcc id "));
		Serial.println(inDccId);
#endif
		ActionsStack::Actions.Add(inDccId, inDccIdAccessory);
		return false;
	}
	*/

	Accessory *acc = this->GetById(inDccId, inDccIdAccessory);

	if (acc == 0)
	{
		return false;
	}

	acc->SetLastDccPosition(inDccId, inDccIdAccessory, inPosition);

#ifdef DEBUG_MODE
	for (int i = 0; i < this->size; i++)
		if (acc == (*this)[i])
		{
		Serial.print(F("MovePosition : Accessory "));
		Serial.print(i);
		Serial.print(F(" DccId "));
		Serial.print(inDccId);
		Serial.print(F(" / "));
		Serial.print(inDccIdAccessory);
		Serial.print(F(" to position "));
		Serial.println(inPosition);
		break;
		}
#endif

	acc->MovePosition(inPosition);

	return true;
}
