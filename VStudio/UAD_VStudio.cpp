// UAD.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "math.h"
#include "arduino.h"

void setup();
void loop();

int _tmain(int argc, _TCHAR* argv[])
{
	pinSetup();
	setup();

	while (true)
		loop();

	return 0;
}

//-----------------------------------------
//			ino PART
//-----------------------------------------
#include "UniversalAccessoryDecoder.h"

//------------------------------------------------------------------------------
// SignalArduino declaration

class SignalArduino : public AccessoryLightMulti
{
public:
	SignalArduino(DriverArduino *inpDriver, byte inNbLeds, int *inpPins, int inFirstPort = 0);
	void SetupSignal(int inStartingDcc);
};

//------------------------------------------------------------------------------
// SignalArduino definition

SignalArduino::SignalArduino(DriverArduino *inpDriver, byte inNbLeds, int *inpPins, int inFirstPort) : AccessoryLightMulti(0, 0, inNbLeds, 0)
{
	for (int led = 0; led < inNbLeds; led++)
	{
		inpDriver->SetupPortMotor(inFirstPort + led, inpPins[led], DIGITAL_INVERTED);
	}

	for (int led = 0; led < inNbLeds; led++)
	{
		// Led number is also port number...
		this->SetupLight(led, inpDriver, inFirstPort + led, 255);
	}
}

void SignalArduino::SetupSignal(int inStartingDcc)
{
	this->Setup();

	// Used dcc codes are
	//                 Led  0    1    2
	// inStartingDcc   / 0  on  off  off
	// inStartingDcc   / 1  off on   off
	// inStartingDcc+1 / 0  off off  on
	// inStartingDcc+1 / 1  off off  off

	this->AdjustDccPositionsSize(this->GetSize() + 1);

	int dcc = inStartingDcc;
	bool etat = false;
	for (int i = 0; i < this->GetSize(); i++)
	{
		if (!etat)
		{
			this->AddDccPosition(dcc, 0, 1 << i, 0);
			etat = true;
		}
		else
		{
			this->AddDccPosition(dcc, 1, 1 << i, 0);
			dcc++;
			etat = false;
		}
	}

	this->AddDccPosition(dcc, etat == true ? 1 : 0, 0, 0);
}

//------------------------------------------------------------------------------
// Classic INO area 
/* kDCC_INTERRUPT values :
Board         int.0   int.1   int.2   int.3   int.4   int.5
Uno, Ethernet   2      3
Mega2560        2      3      21      20      19      18
Leonardo        3      2      0       1       7
*/
#define kDCC_INTERRUPT   0

#define NB_LEDS     3
#define NB_ETATS    3
#define NB_FEUX     1

SignalArduino* signaux[NB_FEUX];

int pins[][NB_ETATS] = {
	{ 5, 6, 7 },
	{ 8, 9, 10 },
	{ 11, 12, 13 },
	/*{ 14, 15, 16 },
	{ 17, 18, 19 },
	{ 20, 21, 22 },
	{ 22, 23, 24 },
	{ 25, 26, 27 },
	{ 28, 29, 30 },
	{ 31, 32, 33 },
	{ 34, 35, 36 },
	{ 37, 38, 39 },
	{ 40, 41, 42 },
	{ 43, 44, 45 },
	{ 46, 47, 48 },
	{ 49, 50, 51 }*/
};

// Accessories
Accessories accessories;
DccCommander dccCommander;
ButtonsCommander buttonsCommander;

// Drivers
DriverArduino *arduino;

int dcc_codes[] = { 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 33, 34, 35, 36};

void setup()
{
	UAD_StartSetup();

	dccCommander.Setup(0x00, 0x00, kDCC_INTERRUPT);
	dccCommander.SetStatusLedPin(13);

	buttonsCommander.Setup(1, new ButtonsCommanderPush(NB_FEUX * NB_ETATS));

	// Ce petit bouton va permettre de passer en revue tous les codes dcc des feux en s�quence...
	int dcc = 0;
	for (int feu = 0; feu < NB_FEUX; feu++)
	{
		PUSH(buttonsCommander, 0)->AddDccId(dcc_codes[dcc], 0);
		PUSH(buttonsCommander, 0)->AddDccId(dcc_codes[dcc], 1);
		PUSH(buttonsCommander, 0)->AddDccId(dcc_codes[dcc+1], 0);
	}

	PUSH(buttonsCommander, 0)->Setup(56);           // port A2

	arduino = new DriverArduino(NB_LEDS * NB_FEUX, 0);
	arduino->Setup();

	accessories.Setup(NB_FEUX);

	for (int feu = 0; feu < NB_FEUX; feu++)
	{
		signaux[feu] = new SignalArduino(arduino, NB_LEDS, pins[feu], feu * 3);
		signaux[feu]->SetupSignal(dcc_codes[feu]);
		accessories.Add(signaux[feu]);
	}

	UAD_EndSetup();

	for (int i = 0; i < 10; i++)
	{
		accessories.DCCToggle(10, 0);
		accessories.Loop();
		delay(1000);
		accessories.DCCToggle(10, 1);
		accessories.Loop();
		delay(1000);
		accessories.DCCToggle(11, 0);
		accessories.Loop();
		delay(1000);
	}
}

void loop()
{
	if (dccCommander.Loop())
	{
		accessories.Loop();
		buttonsCommander.Loop();
	}
}
//-----------------------------------------
//			end ino PART
//-----------------------------------------
